extends Node3D

@onready var track_segments = $TrackSegments

# Called when the node enters the scene tree for the first time.
func _ready():
	FZGlobal.current_track = track_segments
	await get_tree().create_timer(0.1).timeout
	var final_segment := track_segments.get_child(track_segments.get_child_count() - 1) as RoadPath
	for i in 30:
		var ix := i % 6
		var iy := i * 20 + 40
		var tx = remap(ix, 0, 5, -0.6, 0.6)
		var tz = remap(iy, 0, final_segment.segment_length, 1, 0)
		var root_transform := final_segment.road_curve.get_root_transform(tz)
		var surface = final_segment._get_surface(Vector2(tx, tz))
		var new_spawn_mesh := preload("res://asset/scn/common/starting_panel.tscn").instantiate()
		new_spawn_mesh.position = surface.x + surface.y * 0.1
		new_spawn_mesh.basis = root_transform.basis.orthonormalized()
		if i == 25:
				var new_car := preload("res://core/car/car_base.tscn").instantiate()
				new_car.car_position = surface.x
				new_car.orientation = Quaternion(root_transform.basis.orthonormalized())
				new_car.gravity_orientation = Quaternion(root_transform.basis.orthonormalized())
				new_car.orientation = new_car.orientation * Quaternion.from_euler(Vector3(0, deg_to_rad(180), 0))
				new_car.car_definition = preload("res://asset/res/car_definitions/blue_falcon.tres")
				add_child(new_car)
		add_child(new_spawn_mesh)
		#new_spawn_mesh.pla


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
