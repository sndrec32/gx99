@tool

extends Node

var tick_delta : float = 1.0 / Engine.physics_ticks_per_second
var units_to_kmh : float = 260
var kmh_to_units : float = 1.0 / units_to_kmh
var current_track : TrackRoot

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
