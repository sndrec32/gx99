extends EditorNode3DGizmoPlugin

const MyCustomGizmo = preload("res://addons/gx99_track_editing/TrackPathGizmo.gd")

func get_name():
	return "Track Path Gizmo"

func _init():
	create_material("main", Color(1, 1, 1))
	create_handle_material("handles")


func _create_gizmo(node):
	if node is RoadPath or node is RoadPathBezier:
		return MyCustomGizmo.new()
	else:
		return null
