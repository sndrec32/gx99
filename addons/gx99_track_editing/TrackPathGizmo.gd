extends EditorNode3DGizmo

func _redraw():
	clear()

	var node3d := get_node_3d() as RoadPath

	var lines = PackedVector3Array()
	#lines.push_back(Vector3(0, 1, 0))
	#lines.push_back(node3d.road_curve.get_root_transform(Vector3(0, 0, 0)).origin)

	#var handles = PackedVector3Array()
	#var handle_ids := PackedInt32Array()
	var num_segments := ceili(node3d.segment_length * 0.1)
	
	var num_horizontal_lines := 8
	if node3d.road_shape is RoadShapeCylinder or node3d.road_shape is RoadShapePipe:
		num_horizontal_lines = 16
		num_segments = ceili(node3d.segment_length * 0.05)
	
	for i in num_segments:
		for x in num_horizontal_lines:
			var tx := x / float(num_horizontal_lines - 1.0)
			if node3d.road_shape is RoadShapeCylinder or node3d.road_shape is RoadShapePipe:
				tx = x / float(num_horizontal_lines)
			tx *= 2
			tx -= 1
			var point_transform_1 := node3d.road_shape.get_position_at_time(node3d.road_curve, Vector2(tx, (1.0 / num_segments) * i))
			var point_transform_2 := node3d.road_shape.get_position_at_time(node3d.road_curve, Vector2(tx, (1.0 / num_segments) * (i + 1)))
			lines.push_back(point_transform_1)
			lines.push_back(point_transform_2)
	for i in 16:
		for y in num_segments + 1:
			var tx := i / 16.0
			tx *= 2
			tx -= 1
			var ty := float(y) / num_segments
			var point_transform_1 := node3d.road_shape.get_position_at_time(node3d.road_curve, Vector2(tx, ty))
			var point_transform_2 := node3d.road_shape.get_position_at_time(node3d.road_curve, Vector2(tx + 2.0 / 16.0, ty))
			lines.push_back(point_transform_1)
			lines.push_back(point_transform_2)
	
	var line_color := Color.RED
	add_lines(lines, get_plugin().get_material("main"), false, line_color)
