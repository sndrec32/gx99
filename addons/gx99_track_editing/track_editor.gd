@tool
class_name TrackEditor extends EditorPlugin

const custom_gizmo = preload("res://addons/gx99_track_editing/TrackPathGizmoPlugin.gd")

var custom_gizmo_instance := custom_gizmo.new()

# A class member to hold the dock during the plugin life cycle.
var dock : TrackEditorDock
var dock_2 : TrackEditorDock2

#const AUTOLOAD_NAME = "FZGlobalTrackEditor"


var markers : Array[Marker3D] = []

func _enter_tree():
	if !Engine.is_editor_hint():
		return
	add_node_3d_gizmo_plugin(custom_gizmo_instance)
	
	FZGlobalTrackEditor.track_editor = self
	
	dock = load("res://addons/gx99_track_editing/track_editor_dock.tscn").instantiate()
	
	dock.dock_ready.connect(dock_ready)
	
	dock.track_editor_plugin = self
	
	FZGlobalTrackEditor.track_editor_dock = dock
	
	dock_2 = load("res://addons/gx99_track_editing/track_editor_dock_2.tscn").instantiate()
	
	dock_2.dock_ready.connect(dock_ready)
	
	dock_2.track_editor_plugin = self
	
	FZGlobalTrackEditor.track_editor_dock_2 = dock_2
	
	# Add the loaded scene to the docks.
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_SIDE_LEFT, dock)
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_SIDE_RIGHT, dock_2)
	
	#dock.move_to_front()
	# Note that LEFT_UL means the left of the editor, upper-left dock.

func _exit_tree():
	remove_node_3d_gizmo_plugin(custom_gizmo_instance)
	# Clean-up of the plugin goes here.
	# Remove the dock.
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_SIDE_LEFT, dock)
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_SIDE_RIGHT, dock_2)
	#remove_autoload_singleton(AUTOLOAD_NAME)
	# Erase the control from the memory.
	FZGlobalTrackEditor.track_editor = null
	FZGlobalTrackEditor.track_editor_dock = null
	FZGlobalTrackEditor.track_editor_dock_2 = null
	
	dock.free()
	dock_2.free()

func dock_ready() -> void:
	print("fuck")
	#dock.new_track_segment_button.pressed.connect(on_track_segment_button_pressed)

func unproject_position(in_pos : Vector3) -> Vector2:
	return get_editor_interface().get_editor_viewport_3d(0).get_camera_3d().unproject_position(in_pos)

func get_selected_if_roadpath_otherwise_return_null() -> RoadPath:
	var selection := get_editor_interface().get_selection().get_selected_nodes()
	if selection.size() > 0:
		var selected := get_editor_interface().get_selection().get_selected_nodes()[0]
		if selected and is_instance_valid(selected):
			if selected is RoadPath:
				return selected
			elif selected.get_parent() is RoadPath:
				get_editor_interface().get_selection().clear()
				get_editor_interface().get_selection().add_node(selected.get_parent())
				get_editor_interface().edit_node(selected.get_parent())
				return selected.get_parent() as RoadPath
	return null

func add_bezier_track_segment_after(in_path : RoadPath, road_type : ENUMS.ROAD_TYPE) -> void:
	var new_track_piece := RoadPathBezier.new()
	var track_root : TrackRoot
	var first_piece : bool = false
	if in_path:
		track_root = in_path.get_parent() as TrackRoot
	else:
		first_piece = true
		track_root = get_editor_interface().get_selection().get_selected_nodes()[0]
	if !first_piece:
		var latest_track_piece_transform := in_path.road_curve.get_root_transform(1.0)
		track_root.add_child(new_track_piece)
		var id_to_put_above := track_root.get_children().find(in_path)
		track_root.move_child(new_track_piece, id_to_put_above + 1)
		new_track_piece.owner = get_tree().get_edited_scene_root()
		var track_mesh := MeshInstance3D.new()
		new_track_piece.add_child(track_mesh)
		track_mesh.owner = get_tree().get_edited_scene_root()
		new_track_piece.road_curve = CurveMatrix.new()
		new_track_piece.road_curve.clear_all_keyframes()
		match road_type:
			ENUMS.ROAD_TYPE.STANDARD:
				new_track_piece.road_shape = RoadShape.new()
			ENUMS.ROAD_TYPE.PIPE:
				new_track_piece.road_shape = RoadShapePipe.new()
			ENUMS.ROAD_TYPE.CYLINDER:
				new_track_piece.road_shape = RoadShapeCylinder.new()
			ENUMS.ROAD_TYPE.PIPE_OPEN:
				new_track_piece.road_shape = RoadShapePipeOpen.new()
			ENUMS.ROAD_TYPE.CYLINDER_OPEN:
				new_track_piece.road_shape = RoadShapeCylinderOpen.new()
		
		var transform_at_time := in_path.road_curve.get_root_transform(1.0)
		
		var bp1 := BezierControlPoint.new()
		bp1.position = transform_at_time.origin
		bp1.rotation = transform_at_time.basis.orthonormalized()
		bp1.scale = transform_at_time.basis.get_scale()
		bp1.time = 0
		
		DebugDraw3D.draw_position(Transform3D(transform_at_time.basis, transform_at_time.origin), Color( 1, 1, 1), 10)
		DebugDraw3D.draw_position(Transform3D(bp1.rotation * 10, bp1.position), Color(1, 0, 0), 10)
		
		var bp2 := BezierControlPoint.new()
		bp2.position = bp1.position + transform_at_time.basis.z.normalized() * 500
		bp2.rotation = bp1.rotation
		bp2.scale = bp1.scale
		bp2.time = 1
		
		bp1.handle_in = 166
		bp1.handle_out = 166
		bp2.handle_in = 166
		bp2.handle_out = 166
		
		new_track_piece.add_bezier_point(bp1)
		new_track_piece.add_bezier_point(bp2)
	else:
		track_root.add_child(new_track_piece)
		var id_to_put_above := track_root.get_children().find(in_path)
		track_root.move_child(new_track_piece, id_to_put_above + 1)
		new_track_piece.owner = get_tree().get_edited_scene_root()
		var track_mesh := MeshInstance3D.new()
		new_track_piece.add_child(track_mesh)
		track_mesh.owner = get_tree().get_edited_scene_root()
		new_track_piece.road_curve = CurveMatrix.new()
		new_track_piece.road_curve.clear_all_keyframes()
		match road_type:
			ENUMS.ROAD_TYPE.STANDARD:
				new_track_piece.road_shape = RoadShape.new()
			ENUMS.ROAD_TYPE.PIPE:
				new_track_piece.road_shape = RoadShapePipe.new()
			ENUMS.ROAD_TYPE.CYLINDER:
				new_track_piece.road_shape = RoadShapeCylinder.new()
			ENUMS.ROAD_TYPE.PIPE_OPEN:
				new_track_piece.road_shape = RoadShapePipeOpen.new()
			ENUMS.ROAD_TYPE.CYLINDER_OPEN:
				new_track_piece.road_shape = RoadShapeCylinderOpen.new()
		new_track_piece.road_shape.modulation_effect.clear_points()
		new_track_piece.road_shape.modulation_width.clear_points()
	
		var new_global_transform_curve := CurveMatrix.new()
		new_global_transform_curve.position_x.clear_points()
		new_global_transform_curve.position_y.clear_points()
		new_global_transform_curve.position_z.clear_points()
		new_global_transform_curve.rotation_x.clear_points()
		new_global_transform_curve.rotation_y.clear_points()
		new_global_transform_curve.rotation_z.clear_points()
		new_global_transform_curve.scale_x.clear_points()
		new_global_transform_curve.scale_y.clear_points()
		new_global_transform_curve.scale_z.clear_points()
	
		new_track_piece.road_curve.global_transformations.append(new_global_transform_curve)
		new_global_transform_curve.scale_x.add_point(Vector2(0, 1))
		new_global_transform_curve.scale_y.add_point(Vector2(0, 1))
		new_global_transform_curve.scale_z.add_point(Vector2(0, 1))
		new_global_transform_curve.position_x.add_point(Vector2(0, 0))
		new_global_transform_curve.position_y.add_point(Vector2(0, 0))
		new_global_transform_curve.position_z.add_point(Vector2(0, 0))
		new_global_transform_curve.rotation_x.add_point(Vector2(0, 0))
		new_global_transform_curve.rotation_y.add_point(Vector2(0, 0))
		new_global_transform_curve.rotation_z.add_point(Vector2(0, 0))
		
		var bp1 := BezierControlPoint.new()
		bp1.position = Vector3(0,0,0)
		bp1.rotation = Basis.IDENTITY
		bp1.scale = Vector3(45, 45, 1)
		bp1.time = 0
		
		var bp2 := BezierControlPoint.new()
		bp2.position = Vector3(0, 0, 500)
		bp2.rotation = bp1.rotation
		bp2.scale = bp1.scale
		bp2.time = 1
		
		bp1.handle_in = 166
		bp1.handle_out = 166
		bp2.handle_in = 166
		bp2.handle_out = 166
		
		new_track_piece.add_bezier_point(bp1)
		new_track_piece.add_bezier_point(bp2)

func add_regular_track_segment_after(in_path : RoadPath, road_type : ENUMS.ROAD_TYPE) -> void:
	var new_track_piece := RoadPathLine.new()
	var track_root := in_path.get_parent() as TrackRoot
	var latest_track_piece_transform := in_path.road_curve.get_root_transform(1.0)
	track_root.add_child(new_track_piece)
	var id_to_put_above := track_root.get_children().find(in_path)
	track_root.move_child(new_track_piece, id_to_put_above + 1)
	new_track_piece.owner = get_tree().get_edited_scene_root()
	var track_mesh := MeshInstance3D.new()
	new_track_piece.add_child(track_mesh)
	track_mesh.owner = get_tree().get_edited_scene_root()
	new_track_piece.road_curve = CurveMatrix.new()
	match road_type:
		ENUMS.ROAD_TYPE.STANDARD:
			new_track_piece.road_shape = RoadShape.new()
		ENUMS.ROAD_TYPE.PIPE:
			new_track_piece.road_shape = RoadShapePipe.new()
		ENUMS.ROAD_TYPE.CYLINDER:
			new_track_piece.road_shape = RoadShapeCylinder.new()
		ENUMS.ROAD_TYPE.PIPE_OPEN:
			new_track_piece.road_shape = RoadShapePipeOpen.new()
		ENUMS.ROAD_TYPE.CYLINDER_OPEN:
			new_track_piece.road_shape = RoadShapeCylinderOpen.new()
	new_track_piece.start_marker.global_position = latest_track_piece_transform.origin
	new_track_piece.start_marker.global_basis = latest_track_piece_transform.basis.orthonormalized()
	new_track_piece.start_marker.scale = latest_track_piece_transform.basis.get_scale()
	new_track_piece.end_marker.global_position = latest_track_piece_transform.origin + latest_track_piece_transform.basis.orthonormalized().z * 250
	new_track_piece.end_marker.global_basis = latest_track_piece_transform.basis.orthonormalized()
	new_track_piece.end_marker.scale = latest_track_piece_transform.basis.get_scale()
