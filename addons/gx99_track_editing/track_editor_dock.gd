@tool

class_name TrackEditorDock extends Control

signal dock_ready

var track_editor_plugin : TrackEditor

var track_root : TrackRoot

@onready var main_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/MainButtons
@onready var new_track_segment_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/NewTrackSegmentButtons
@onready var new_track_segment_type_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/NewTrackSegmentTypeButtons
@onready var new_embed_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/NewEmbedButtons
@onready var new_track_object_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/NewTrackObjectButtons
@onready var bezier_buttons: VBoxContainer = $MainGUI/VBoxContainer/MainGUIMargin/MainGUIHBox/BezierButtons

@onready var outliner: VBoxContainer = $MainGUI/VBoxContainer/ScrollContainer/Outliner


var desired_road_type := ENUMS.ROAD_TYPE.STANDARD

# Called when the node enters the scene tree for the first time.
func _ready():
	dock_ready.emit()
	EditorInterface.get_selection().selection_changed.connect(update_outliner)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !track_root:
		var scene_root := get_tree().edited_scene_root
		if !scene_root:
			return
		for child in scene_root.get_children():
			if child is TrackRoot:
				track_root = child
				break
	
	if !track_root:
		return
	
	if EditorInterface.get_selection().get_selected_nodes().size() == 0:
		return
	if bezier_buttons.visible and !(EditorInterface.get_selection().get_selected_nodes()[0] is Marker3D):
		main_buttons.visible = true
		bezier_buttons.visible = false
	if EditorInterface.get_selection().get_selected_nodes()[0] is Marker3D:
		if EditorInterface.get_selection().get_selected_nodes()[0].get_parent() is RoadPathBezier:
			var dock := FZGlobalTrackEditor.track_editor_dock as TrackEditorDock
			bezier_buttons.visible = true
			main_buttons.visible = false
			new_track_segment_buttons.visible = false
			new_embed_buttons.visible = false
			new_track_object_buttons.visible = false
		else:
			var dock := FZGlobalTrackEditor.track_editor_dock as TrackEditorDock
			bezier_buttons.visible = false
			main_buttons.visible = false
			new_track_segment_buttons.visible = false
			new_embed_buttons.visible = false
			new_track_object_buttons.visible = false

func update_outliner() -> void:
	if !track_root:
		return
	for child in outliner.get_children():
		child.queue_free()
	for i in track_root.get_child_count():
		var child = track_root.get_child(i)
		if child is RoadPath:
			var new_button := Button.new()
			new_button.text = "Track Segment " + str(i + 1)
			new_button.pressed.connect(select_node.bind(child))
			outliner.add_child(new_button)
			if EditorInterface.get_selection().get_selected_nodes().has(child):
				new_button.modulate = Color(2.0, 2.0, 1.0)

func select_node(in_node : Node) -> void:
	EditorInterface.get_selection().clear()
	EditorInterface.get_selection().add_node(in_node)

func _on_new_track_segment_pressed():
	main_buttons.visible = false
	new_track_segment_buttons.visible = true

func _on_add_embed_pressed():
	main_buttons.visible = false
	new_embed_buttons.visible = true

func _on_new_track_object_pressed():
	main_buttons.visible = false
	new_track_object_buttons.visible = true

func _on_edit_segment_props_pressed():
	pass # Replace with function body.


func _on_edit_track_props_pressed():
	pass # Replace with function body.


func _on_road_standard_pressed():
	new_track_segment_buttons.visible = false
	new_track_segment_type_buttons.visible = true
	desired_road_type = ENUMS.ROAD_TYPE.STANDARD


func _on_road_pipe_pressed():
	new_track_segment_buttons.visible = false
	new_track_segment_type_buttons.visible = true
	desired_road_type = ENUMS.ROAD_TYPE.PIPE



func _on_road_cylinder_pressed():
	new_track_segment_buttons.visible = false
	new_track_segment_type_buttons.visible = true
	desired_road_type = ENUMS.ROAD_TYPE.CYLINDER

func _on_road_cylinder_open_pressed() -> void:
	new_track_segment_buttons.visible = false
	new_track_segment_type_buttons.visible = true
	desired_road_type = ENUMS.ROAD_TYPE.CYLINDER_OPEN

func _on_road_pipe_open_pressed() -> void:
	new_track_segment_buttons.visible = false
	new_track_segment_type_buttons.visible = true
	desired_road_type = ENUMS.ROAD_TYPE.PIPE_OPEN

func retrieve_point_int_from_string(in_name : String) -> int:
	var cut := in_name.erase(0, 6)
	var space_pos := cut.find(" ")
	var truncate := cut
	if space_pos != -1:
		truncate = cut.erase(space_pos, 100)
	var num = int(truncate)
	return num

func _on_add_point_after_pressed():
	var sel = EditorInterface.get_selection().get_selected_nodes()[0]
	if sel is Marker3D:
		var pt_handle : Marker3D = sel as Marker3D
		var num = retrieve_point_int_from_string(pt_handle.name)
		var bez_seg := sel.get_parent() as RoadPathBezier
		var new_bezier := BezierControlPoint.new()
		var num_pts := bez_seg.bezier_points.size()
		if num == num_pts:
			var lst_point := bez_seg.bezier_points[num_pts - 1]
			var prev_point := bez_seg.bezier_points[num_pts - 2]
			lst_point.time = (lst_point.time - prev_point.time) * 0.5 + prev_point.time
			new_bezier.time = 1.0
			new_bezier.position = lst_point.position + lst_point.rotation.z.normalized() * 500
			new_bezier.handle_in = 125
			new_bezier.handle_out = 125
			new_bezier.scale = lst_point.scale
			new_bezier.rotation = lst_point.rotation
		else:
			var prv_point := bez_seg.bezier_points[num - 1]
			var nxt_point := bez_seg.bezier_points[num]
			prv_point.handle_out *= 0.333
			nxt_point.handle_in *= 0.333
			new_bezier.time = (prv_point.time + nxt_point.time) * 0.5
			var point_transform = bez_seg.sample_bezier_at_time(new_bezier.time)
			new_bezier.position = point_transform.origin
			new_bezier.rotation = point_transform.basis.orthonormalized()
			new_bezier.scale = point_transform.basis.get_scale()
			new_bezier.handle_in = prv_point.handle_out
			new_bezier.handle_out = nxt_point.handle_in
				
		bez_seg.add_bezier_point(new_bezier)


func _on_add_point_before_button_down():
	pass # Replace with function body.


func _on_delete_point_button_down():
	var sel = EditorInterface.get_selection().get_selected_nodes()[0]
	if sel is Marker3D:
		var pt_handle : Marker3D = sel as Marker3D
		var num = retrieve_point_int_from_string(pt_handle.name)
		var bez_seg := sel.get_parent() as RoadPathBezier
		bez_seg.remove_bezier_point_at_index(num - 1)


func _on_road_line_pressed() -> void:
	var selected : RoadPath = track_editor_plugin.get_selected_if_roadpath_otherwise_return_null()
	if selected:
		track_editor_plugin.add_regular_track_segment_after(selected, desired_road_type)
	else:
		track_editor_plugin.add_regular_track_segment_after(track_root.get_child(track_root.get_child_count() - 1), desired_road_type)
	new_track_segment_type_buttons.visible = false



func _on_road_bezier_pressed() -> void:
	var selected : RoadPath = track_editor_plugin.get_selected_if_roadpath_otherwise_return_null()
	if selected:
		track_editor_plugin.add_bezier_track_segment_after(selected, desired_road_type)
	else:
		track_editor_plugin.add_bezier_track_segment_after(track_root.get_child(track_root.get_child_count() - 1), desired_road_type)
	new_track_segment_type_buttons.visible = false


func _on_road_curve_pressed() -> void:
	new_track_segment_type_buttons.visible = false
