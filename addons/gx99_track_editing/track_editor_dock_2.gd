@tool

class_name TrackEditorDock2 extends Control

signal dock_ready

var track_editor_plugin : TrackEditor

var track_root : TrackRoot

@onready var tab_container: TabContainer = $Control/TabContainer
@onready var track_cross_section_slider: HSlider = $Control/TabContainer/Info/VBoxContainer/TrackCrossSectionSlider
@onready var smooth_curve: Line2D = $Control/TabContainer/Info/VBoxContainer/ColorRect/SmoothCurve
@onready var poly_curve: Line2D = $Control/TabContainer/Info/VBoxContainer/ColorRect/PolyCurve

@onready var modulation_dropdown: OptionButton = $Control/TabContainer/Modulation/HBoxContainer/ModulationDropdown
@onready var new_modulation: Button = $Control/TabContainer/Modulation/HBoxContainer/NewModulation
@onready var mod_curve_effect: CurveEditor = $Control/TabContainer/Modulation/ModCurveEffect
@onready var mod_curve_height: CurveEditor = $Control/TabContainer/Modulation/ModCurveHeight

@onready var embed_dropdown: OptionButton = $Control/TabContainer/Embeds/HBoxContainer/EmbedDropdown
@onready var new_embed: Button = $Control/TabContainer/Embeds/HBoxContainer/NewEmbed
@onready var embed_type: OptionButton = $Control/TabContainer/Embeds/EmbedType
@onready var embed_start: HSlider = $Control/TabContainer/Embeds/EmbedStart
@onready var embed_end: HSlider = $Control/TabContainer/Embeds/EmbedEnd
@onready var embed_curve_left: CurveEditor = $Control/TabContainer/Embeds/EmbedCurveLeft
@onready var embed_curve_right: CurveEditor = $Control/TabContainer/Embeds/EmbedCurveRight

@onready var draw_mesh: CheckBox = $Control/VBoxContainer/DrawMesh
@onready var draw_curve: CheckBox = $Control/VBoxContainer/DrawCurve
@onready var draw_handles: CheckBox = $Control/VBoxContainer/DrawHandles


func _ready():
	dock_ready.emit()
	track_cross_section_slider.value_changed.connect(update_cross_section_lines)
	EditorInterface.get_selection().selection_changed.connect(update_cross_section_lines)

func _process(delta: float) -> void:
	if !track_root:
		var scene_root := get_tree().edited_scene_root
		if !scene_root:
			return
		for child in scene_root.get_children():
			if child is TrackRoot:
				track_root = child
				break
	
	if !track_root:
		return
	
	if EditorInterface.get_selection().get_selected_nodes().size() == 0:
		tab_container.visible = false
		return
	
	var editor_cam:Camera3D = EditorInterface.get_editor_viewport_3d(0).get_camera_3d()
	var selected = EditorInterface.get_selection().get_selected_nodes()[0]
	var path : RoadPath
	if selected is RoadPath:
		path = selected
	if selected.get_parent() is RoadPath:
		path = selected.get_parent()
	if path:
		tab_container.visible = true
		var base_pos : Vector3 = path.road_curve.get_root_transform(track_cross_section_slider.value).origin
		var segments := 256.0
		DebugDraw3D.scoped_config().set_thickness(base_pos.distance_to(editor_cam.global_position) * 0.0025)
		for i in segments - 1:
			var t := Vector2((i / (segments - 1) * 2.0) - 1.0, track_cross_section_slider.value)
			var t_2 := Vector2(((i + 1) / (segments - 1) * 2.0) - 1.0, track_cross_section_slider.value)
			var point : Vector3 = path.road_shape.get_position_at_time(path.road_curve, t)
			var point_2 : Vector3 = path.road_shape.get_position_at_time(path.road_curve, t_2)
			DebugDraw3D.draw_line(point, point_2, Color.RED, delta)
	else:
		tab_container.visible = false

func update_cross_section_lines(in_new_value : float = track_cross_section_slider.value) -> void:
	smooth_curve.clear_points()
	poly_curve.clear_points()
	var selected = EditorInterface.get_selection().get_selected_nodes()
	if selected.size() == 0:
		return
	var path : RoadPath
	if selected is RoadPath:
		path = selected
	if selected.get_parent() is RoadPath:
		path = selected.get_parent()
	if !path:
		return
	var width := 315.0
	var half_width := width * 0.5
	var largest := 0.0
	if path is RoadPath:
		for i in width:
			var t := Vector2((i / (width - 1) * 2.0) - 1.0, in_new_value)
			var point : Vector3 = path.road_shape.get_position_at_time(path.road_curve, t)
			var transform : Transform3D = path.road_curve.get_root_transform(t.y)
			var t_scale := transform.basis.get_scale()
			point = transform.affine_inverse() * point
			if path.road_shape is RoadShapeCylinder or path.road_shape is RoadShapeCylinderOpen or path.road_shape is RoadShapePipe or path.road_shape is RoadShapePipeOpen:
				point *= t_scale / maxf(t_scale.x, t_scale.y)
			else:
				point *= t_scale / t_scale.x
			if maxf(point.x, point.y) > largest:
				largest = maxf(point.x, point.y)
			var flat_point := Vector2(point.x, point.y * -1) * half_width
			#print(flat_point)
			smooth_curve.add_point(flat_point)
		for i in path.horizontal_road_mesh_segments.size():
			var t := Vector2(path.horizontal_road_mesh_segments[i] * 2.0 - 1.0, in_new_value)
			var point : Vector3 = path.road_shape.get_position_at_time(path.road_curve, t)
			var transform : Transform3D = path.road_curve.get_root_transform(t.y)
			var t_scale := transform.basis.get_scale()
			point = transform.affine_inverse() * point
			if path.road_shape is RoadShapeCylinder or path.road_shape is RoadShapeCylinderOpen or path.road_shape is RoadShapePipe or path.road_shape is RoadShapePipeOpen:
				point *= t_scale / maxf(t_scale.x, t_scale.y)
			else:
				point *= t_scale / t_scale.x
			
			#point.y *= maxf(t_scale.y, t_scale.x) / minf(t_scale.y, t_scale.x)
			var flat_point := Vector2(point.x, point.y * -1) * half_width
			#print(flat_point)
			poly_curve.add_point(flat_point)
		smooth_curve.scale = Vector2.ONE / largest
		poly_curve.scale = Vector2.ONE / largest
		smooth_curve.width = 2.0 * largest
		poly_curve.width = 2.0 * largest
