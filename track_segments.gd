@tool
class_name TrackRoot extends Node3D

@export var gen_checkpoints : bool = false:
	set(new_bool):
		gen_checkpoints = false
		if new_bool:
			_generate_checkpoints()

@export var checkpoints : Array[Checkpoint] = []

func _generate_checkpoints():
	checkpoints.clear()
	for ch in get_children().size():
		print(ch)
		var child := get_child(ch) as RoadPath
		print(child)
		var num_checks := child.num_checkpoints
		for i in num_checks + 1:
			var ty := (1.0 / (num_checks + 1)) * i
			var ty2 := ty + (1.0 / (num_checks + 1))
			var tf_1 := child.road_curve.get_root_transform(ty)
			var tf_2 := child.road_curve.get_root_transform(ty2)
			var cp_x_rad_1 := tf_1.basis.x.length()
			var cp_x_rad_2 := tf_2.basis.x.length()
			var cp_y_rad_1 := tf_1.basis.y.length()
			var cp_y_rad_2 := tf_2.basis.y.length()
			var cp_dist := (tf_2.origin - tf_1.origin).length()
			var new_checkpoint = Checkpoint.new(tf_1.origin, tf_2.origin, tf_1.basis.orthonormalized(), tf_2.basis.orthonormalized(), cp_x_rad_1, cp_x_rad_2, cp_y_rad_1, cp_y_rad_2, ty, ty2, cp_dist, ch)
			checkpoints.append(new_checkpoint)

func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if false:
		return
	
	if !Engine.is_editor_hint():
		return
	if EditorInterface.get_selection().get_selected_nodes().size() > 0 and EditorInterface.get_selection().get_selected_nodes()[0] == self:
		for cp in checkpoints:
			var render_basis := cp.orientation_start.rotated(cp.orientation_start.x, PI * 0.5)
			render_basis.x *= cp.x_radius_start
			render_basis.z *= cp.y_radius_start
			render_basis.y = render_basis.y.normalized() * 0.25
			DebugDraw3D.draw_cylinder(Transform3D(render_basis, cp.position_start + render_basis.y * 0.5), Color(0, 1, 0, 0.1), delta)
			#DebugDraw3D.draw_plane(Plane(cp.orientation_start.z, cp.position_start), Color(0, 1, 0, 0.05), cp.position_start, delta)
			render_basis = cp.orientation_end.rotated(cp.orientation_end.x, PI * 0.5)
			render_basis.x *= cp.x_radius_end
			render_basis.z *= cp.y_radius_end
			render_basis.y = render_basis.y.normalized() * 0.25
			DebugDraw3D.draw_cylinder(Transform3D(render_basis, cp.position_end - render_basis.y * 0.5), Color(1, 0, 0, 0.1), delta)
			#DebugDraw3D.draw_plane(Plane(cp.orientation_end.z, cp.position_end), Color(1, 0, 0, 0.05), cp.position_end, delta)
