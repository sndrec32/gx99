@tool

class_name RoadPath extends Node3D

@export var road_shape : RoadShape = RoadShape.new()
@export var road_curve : CurveMatrix = CurveMatrix.new()
@export var road_style : Mesh
@export var road_uv_multiplier := 1.0
@export var horizontal_road_mesh_segments : PackedFloat32Array
@export var num_checkpoints := 8:
	set(in_checkpoint_count):
		num_checkpoints = in_checkpoint_count
		print(num_checkpoints)

@export var segment_length := 0.0

static var plugin

class PathInspector extends EditorInspectorPlugin:
	func _can_handle(object): return object is RoadPath
	
	func _parse_begin(object):
		var cpc = object as RoadPath
		
		var button_2 = Button.new()
		button_2.text = "Refresh Mesh"
		button_2.pressed.connect(cpc._try_generate_mesh)
		
		var box_container = VBoxContainer.new()
		box_container.add_child(button_2)
		
		var container = MarginContainer.new()
		container.add_theme_constant_override("margin_bottom", 10)
		container.add_child(box_container)
		
		add_custom_control(container)

func _enter_tree():
	if Engine.is_editor_hint():
		if plugin == null:
			plugin = EditorPlugin.new()
			plugin.add_inspector_plugin(PathInspector.new())

func _ready():
	_try_generate_mesh()
	if horizontal_road_mesh_segments == null:
		horizontal_road_mesh_segments = PackedFloat32Array()
		for i in 5:
			horizontal_road_mesh_segments.append(0.25 * i)

func _get_surface( in_t : Vector2 ) -> Basis:
	var surface := road_shape.get_surface_at_time(road_curve, in_t)
	return Basis(surface[0], surface[1], Vector3.ZERO)

func _try_generate_mesh() -> void:
	
	horizontal_road_mesh_segments.sort()
	
	var y_times : PackedFloat32Array = [0.0]
	var start_transform = road_curve.get_root_transform(0)
	var orientations : Array[Quaternion] = [start_transform.basis.get_rotation_quaternion()]
	var scales : PackedVector3Array = [start_transform.basis.get_scale()]
	var dists : PackedFloat32Array = [0.0]
	var length_subdivide_dist := 30.0
	var min_angle_difference := deg_to_rad(2.0)
	var total_dist := 0.0
	
	var iter := 0
	while iter <= 1000:
		var current_y_time := 0.001 * iter
		var prev_sample_point := road_curve.get_root_transform(y_times[y_times.size() - 1])
		var sample_point := road_curve.get_root_transform(current_y_time)
		var iter_dist := sample_point.origin.distance_to(prev_sample_point.origin)
		var prev_rot := prev_sample_point.basis.get_rotation_quaternion()
		var cur_rot := sample_point.basis.get_rotation_quaternion()
		var prev_scale := prev_sample_point.basis.get_scale()
		var cur_scale := sample_point.basis.get_scale()
		var scale_changed := false
		if absf(prev_scale.x - cur_scale.x) > 1.0:
			scale_changed = true
		if absf(prev_scale.y - cur_scale.y) > 1.0:
			scale_changed = true
		if absf(prev_scale.z - cur_scale.z) > 1.0:
			scale_changed = true
		if iter_dist >= length_subdivide_dist or iter == 1000 or scale_changed or prev_rot.angle_to(cur_rot) > min_angle_difference:
			total_dist += iter_dist
			dists.append(total_dist)
			y_times.append(current_y_time)
			orientations.append(cur_rot)
		iter += 1
	segment_length = total_dist
	
	var x_segments := horizontal_road_mesh_segments.size()
	
	if !road_style:
		var subdivision := y_times.size() - 1
		var vertices := [[]]
		var normals := [[]]
		var uvs := [[]]
		
		vertices.resize(x_segments)
		normals.resize(x_segments)
		uvs.resize(x_segments)
		
		for x in x_segments:
			vertices[x] = []
			normals[x] = []
			uvs[x] = []
			vertices[x].resize(subdivision + 1)
			normals[x].resize(subdivision + 1)
			uvs[x].resize(subdivision + 1)
		
		var dists_for_uv : PackedFloat32Array = dists.duplicate()
		for i in dists_for_uv.size():
			dists_for_uv[i] = dists_for_uv[i] / (128 / road_uv_multiplier)
			dists_for_uv[i] = remap(dists_for_uv[i], 0, total_dist / (128 / road_uv_multiplier), 0, ceilf(total_dist / (128 / road_uv_multiplier)))
		
		for x in x_segments:
			for y in subdivision + 1:
				var t = Vector2(horizontal_road_mesh_segments[x] * 2.0 - 1.0, y_times[y])
				var pos_normal : PackedVector3Array = road_shape.get_surface_at_time(road_curve, t)
				vertices[x][y] = pos_normal[0]
				normals[x][y] = pos_normal[1]
				var uvx := horizontal_road_mesh_segments[x]
				var uvy := dists_for_uv[y]
				uvs[x][y] = Vector2(uvx, uvy)
				#DebugDraw3D.draw_plane(Plane(new_transform.origin, new_transform.origin + new_transform.basis.y), Color(1, 1, 1), Vector3(0, 0, 0), delta)
				#DebugDraw3D.draw_grid_xf(new_transform.scaled_local(Vector3.ONE * 1), Vector2i.ONE, Color(1, 1, 1, 1), true, delta)
				#DebugDraw3D.draw_arrow(pos_normal[0], pos_normal[0] + pos_normal[1] * 5, Color(1, 0, 0), 1, true, 5)
		
		var triangle_vertices : PackedVector3Array = []
		var triangle_normals : PackedVector3Array = []
		var triangle_uvs : PackedVector2Array = []
		
		for x in vertices.size():
			for y in vertices[x].size() - 1:
				if x < vertices.size() - 1:
					var v3 : Vector3 = vertices[x][y]
					var n3 : Vector3 = normals[x][y]
					var uv3 : Vector2 = uvs[x][y]
					var v2 : Vector3 = vertices[x][y + 1]
					var n2 : Vector3 = normals[x][y + 1]
					var uv2 : Vector2 = uvs[x][y + 1]
					var v1 : Vector3 = vertices[x + 1][y]
					var n1 : Vector3 = normals[x + 1][y]
					var uv1 : Vector2 = uvs[x + 1][y]
					triangle_vertices.push_back(v1)
					triangle_vertices.push_back(v2)
					triangle_vertices.push_back(v3)
					triangle_normals.push_back(n1)
					triangle_normals.push_back(n2)
					triangle_normals.push_back(n3)
					triangle_uvs.push_back(uv1)
					triangle_uvs.push_back(uv2)
					triangle_uvs.push_back(uv3)
				if x > 0:
					var v3 : Vector3 = vertices[x][y]
					var n3 : Vector3 = normals[x][y]
					var uv3 : Vector2 = uvs[x][y]
					var v2 : Vector3 = vertices[x - 1][y + 1]
					var n2 : Vector3 = normals[x - 1][y + 1]
					var uv2 : Vector2 = uvs[x - 1][y + 1]
					var v1 : Vector3 = vertices[x][y + 1]
					var n1 : Vector3 = normals[x][y + 1]
					var uv1 : Vector2 = uvs[x][y + 1]
					triangle_vertices.push_back(v1)
					triangle_vertices.push_back(v2)
					triangle_vertices.push_back(v3)
					triangle_normals.push_back(n1)
					triangle_normals.push_back(n2)
					triangle_normals.push_back(n3)
					triangle_uvs.push_back(uv1)
					triangle_uvs.push_back(uv2)
					triangle_uvs.push_back(uv3)
		
		var arr_mesh = ArrayMesh.new()
		var arrays = []
		arrays.resize(Mesh.ARRAY_MAX)
		arrays[Mesh.ARRAY_VERTEX] = triangle_vertices
		arrays[Mesh.ARRAY_NORMAL] = triangle_normals
		arrays[Mesh.ARRAY_TEX_UV] = triangle_uvs
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
		for ch in get_children():
			if ch is MeshInstance3D:
				ch.mesh = arr_mesh
				break
	else:
		var total_copies := ceilf(total_dist / (128 / road_uv_multiplier))
		var new_mesh := ArrayMesh.new()
		for surface in road_style.get_surface_count():
			var surface_arrays := road_style.surface_get_arrays(surface)
			var new_arrays := surface_arrays.duplicate(true)
			var vertex_count : int = surface_arrays[Mesh.ARRAY_VERTEX].size()
			new_arrays[Mesh.ARRAY_VERTEX].clear()
			new_arrays[Mesh.ARRAY_NORMAL].clear()
			new_arrays[Mesh.ARRAY_TEX_UV].clear()
			new_arrays[Mesh.ARRAY_TANGENT].clear()
			if new_arrays[Mesh.ARRAY_COLOR]:
				new_arrays[Mesh.ARRAY_COLOR].clear()
			new_arrays[Mesh.ARRAY_INDEX].clear()
			var indices : PackedInt32Array = surface_arrays[Mesh.ARRAY_INDEX]
			var largest := 0
			for i in indices:
				if i > largest:
					largest = i
			#print(largest)
			#print(surface_arrays[Mesh.ARRAY_VERTEX].size())
			for i in total_copies:
				for v in vertex_count:
					var new_vertex : Vector3 = surface_arrays[Mesh.ARRAY_VERTEX][v] + Vector3(0, 0, i)
					new_vertex.z = new_vertex.z / (total_copies - 1)
					var vertex_t := Vector2(new_vertex.x * 2.0, new_vertex.z)
					#print(vertex_t)
					var point_transform := road_shape.get_transform_at_time(road_curve, vertex_t)
					var root_transform := road_curve.get_root_transform(vertex_t.y)
					new_vertex.y *= maxf(root_transform.basis.x.length(), root_transform.basis.y.length())
					if vertex_t.y == 1.0:
						DebugDraw3D.draw_sphere(point_transform.origin, 0.5, Color.RED, 5)
						DebugDraw3D.draw_sphere(root_transform.origin, 1.0, Color.GREEN, 5)
						DebugDraw3D.draw_sphere(new_vertex, 0.5, Color.BLUE, 5)
					new_vertex = point_transform * new_vertex
					new_arrays[Mesh.ARRAY_VERTEX].push_back(new_vertex)
					var new_normal : Vector3 = surface_arrays[Mesh.ARRAY_NORMAL][v]
					new_normal = point_transform.basis.orthonormalized() * new_normal
					new_arrays[Mesh.ARRAY_NORMAL].push_back(new_normal)
					new_arrays[Mesh.ARRAY_TEX_UV].push_back(surface_arrays[Mesh.ARRAY_TEX_UV][v])
					new_arrays[Mesh.ARRAY_TANGENT].push_back(surface_arrays[Mesh.ARRAY_TANGENT][v])
					new_arrays[Mesh.ARRAY_TANGENT].push_back(surface_arrays[Mesh.ARRAY_TANGENT][v + 1])
					new_arrays[Mesh.ARRAY_TANGENT].push_back(surface_arrays[Mesh.ARRAY_TANGENT][v + 2])
					new_arrays[Mesh.ARRAY_TANGENT].push_back(surface_arrays[Mesh.ARRAY_TANGENT][v + 3])
					if new_arrays[Mesh.ARRAY_COLOR]:
						new_arrays[Mesh.ARRAY_COLOR].push_back(surface_arrays[Mesh.ARRAY_COLOR][v])
				for v in indices.size():
					new_arrays[Mesh.ARRAY_INDEX].push_back(surface_arrays[Mesh.ARRAY_INDEX][v] + surface_arrays[Mesh.ARRAY_VERTEX].size() * i)
					
			new_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, new_arrays)
		for ch in get_children():
			if ch is MeshInstance3D:
				ch.mesh = new_mesh
				break

var last_gen_time := 0

func _process(delta):
	if !(road_shape and road_curve):
		return
	if !(road_curve.position_x):
		return
	if !Engine.is_editor_hint():
		return
	
	
	
	if Time.get_ticks_msec() > last_gen_time + 100:
		last_gen_time = Time.get_ticks_msec()
		if EditorInterface.get_selection().get_selected_nodes().size() > 0 and EditorInterface.get_selection().get_selected_nodes()[0] == self:
			for gizmo:EditorNode3DGizmo in get_gizmos():
				gizmo._redraw()
