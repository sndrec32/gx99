@tool

class_name CurveMatrix extends Resource

@export var global_transformations : Array[CurveMatrix] = []
@export var position_x : Curve
@export var position_y : Curve
@export var position_z : Curve
@export var rotation_xx : Curve
@export var rotation_xy : Curve
@export var rotation_xz : Curve
@export var rotation_yx : Curve
@export var rotation_yy : Curve
@export var rotation_yz : Curve
@export var rotation_zx : Curve
@export var rotation_zy : Curve
@export var rotation_zz : Curve
@export var scale_x : Curve
@export var scale_y : Curve
@export var scale_z : Curve

func clear_all_keyframes() -> void:
	position_x.clear_points()
	position_y.clear_points()
	position_z.clear_points()
	rotation_xx.clear_points()
	rotation_xy.clear_points()
	rotation_xz.clear_points()
	rotation_yx.clear_points()
	rotation_yy.clear_points()
	rotation_yz.clear_points()
	rotation_zx.clear_points()
	rotation_zy.clear_points()
	rotation_zz.clear_points()
	scale_x.clear_points()
	scale_y.clear_points()
	scale_z.clear_points()

func key_pos_at_time(in_pos : Vector3, in_t : float) -> void:
	position_x.add_point(Vector2(in_t, in_pos.x))
	position_y.add_point(Vector2(in_t, in_pos.y))
	position_z.add_point(Vector2(in_t, in_pos.z))

func key_rot_at_time(in_rot : Basis, in_t : float) -> void:
	in_rot = in_rot.orthonormalized()
	rotation_xx.add_point(Vector2(in_t, in_rot.x.x))
	rotation_xy.add_point(Vector2(in_t, in_rot.x.y))
	rotation_xz.add_point(Vector2(in_t, in_rot.x.z))
	rotation_yx.add_point(Vector2(in_t, in_rot.y.x))
	rotation_yy.add_point(Vector2(in_t, in_rot.y.y))
	rotation_yz.add_point(Vector2(in_t, in_rot.y.z))
	rotation_zx.add_point(Vector2(in_t, in_rot.z.x))
	rotation_zy.add_point(Vector2(in_t, in_rot.z.y))
	rotation_zz.add_point(Vector2(in_t, in_rot.z.z))
	
func key_scale_at_time(in_scale : Vector3, in_t : float) -> void:
	scale_x.add_point(Vector2(in_t, in_scale.x))
	scale_y.add_point(Vector2(in_t, in_scale.y))
	scale_z.add_point(Vector2(in_t, in_scale.z))
	
func key_transform_at_time(in_pos : Vector3, in_rot : Basis, in_scale : Vector3, in_t : float) -> void:
	key_pos_at_time(in_pos, in_t)
	key_rot_at_time(in_rot, in_t)
	key_scale_at_time(in_scale, in_t)
	make_all_keys_linear()

func smooth_curve(in_curve : Curve) -> void:
	var lowest = in_curve.get_point_position(0).y
	var highest = in_curve.get_point_position(0).y
	for p in in_curve.point_count:
		if in_curve.get_point_position(p).y < lowest:
			lowest = in_curve.get_point_position(p).y
		if in_curve.get_point_position(p).y > highest:
			highest = in_curve.get_point_position(p).y
		in_curve.set_point_left_mode(p, Curve.TANGENT_FREE)
		in_curve.set_point_right_mode(p, Curve.TANGENT_FREE)
		if p == 0:
			var p1 : Vector2 = in_curve.get_point_position(p)
			var p2 : Vector2 = in_curve.get_point_position(p + 1)
			var tan := (p2 - p1).normalized() * 0.5
			var ang := atan2(tan.y, tan.x)
			in_curve.set_point_right_tangent(p, tan(ang * 2))
		elif p == in_curve.point_count - 1:
			var p1 : Vector2 = in_curve.get_point_position(p - 1)
			var p2 : Vector2 = in_curve.get_point_position(p)
			var ang : float = atan2(p2.y - p1.y, p2.x - p1.x)
			var tan : float = tan(ang)
			in_curve.set_point_left_tangent(p, tan(ang * 2))
		else:
			var p1 : Vector2 = in_curve.get_point_position(p - 1)
			var p2 : Vector2 = in_curve.get_point_position(p)
			var p3 : Vector2 = in_curve.get_point_position(p + 1)
			var tan : Vector2 = ((p2 - p1).normalized() + (p3 - p2).normalized()) * 0.5
			var ang : float = atan2(tan.y, tan.x)
			in_curve.set_point_left_tangent(p, ang)
			in_curve.set_point_right_tangent(p, ang)
	in_curve.max_value = highest
	in_curve.min_value = lowest

func make_curve_linear(in_curve : Curve) -> void:
	for p in in_curve.point_count:
		in_curve.set_point_left_mode(p, Curve.TANGENT_LINEAR)
		in_curve.set_point_right_mode(p, Curve.TANGENT_LINEAR)

func make_all_keys_linear() -> void:
	make_curve_linear(position_x)
	make_curve_linear(position_y)
	make_curve_linear(position_z)
	make_curve_linear(rotation_xx)
	make_curve_linear(rotation_xy)
	make_curve_linear(rotation_xz)
	make_curve_linear(rotation_yx)
	make_curve_linear(rotation_yy)
	make_curve_linear(rotation_yz)
	make_curve_linear(rotation_zx)
	make_curve_linear(rotation_zy)
	make_curve_linear(rotation_zz)
	make_curve_linear(scale_x)
	make_curve_linear(scale_y)
	make_curve_linear(scale_z)

func auto_smooth_all_keys() -> void:
	smooth_curve(position_x)
	smooth_curve(position_y)
	smooth_curve(position_z)
	smooth_curve(rotation_xx)
	smooth_curve(rotation_xy)
	smooth_curve(rotation_xz)
	smooth_curve(rotation_yx)
	smooth_curve(rotation_yy)
	smooth_curve(rotation_yz)
	smooth_curve(rotation_zx)
	smooth_curve(rotation_zy)
	smooth_curve(rotation_zz)
	smooth_curve(scale_x)
	smooth_curve(scale_y)
	smooth_curve(scale_z)
	

func _init(p_position_x : Curve = Curve.new(), p_position_y : Curve = Curve.new(), p_position_z : Curve = Curve.new(), p_rotation_xx : Curve = Curve.new(), p_rotation_xy : Curve = Curve.new(), p_rotation_xz : Curve = Curve.new(), p_rotation_yx : Curve = Curve.new(), p_rotation_yy : Curve = Curve.new(), p_rotation_yz : Curve = Curve.new(), p_rotation_zx : Curve = Curve.new(), p_rotation_zy : Curve = Curve.new(), p_rotation_zz : Curve = Curve.new(), p_scale_x : Curve = Curve.new(), p_scale_y : Curve = Curve.new(), p_scale_z : Curve = Curve.new()):
	p_position_z.max_value = 500
	p_scale_x.max_value = 90
	p_scale_y.max_value = 90
	
	p_position_x.clear_points()
	p_position_y.clear_points()
	p_position_z.clear_points()
	p_rotation_xx.clear_points()
	p_rotation_xy.clear_points()
	p_rotation_xz.clear_points()
	p_rotation_yx.clear_points()
	p_rotation_yy.clear_points()
	p_rotation_yz.clear_points()
	p_rotation_zx.clear_points()
	p_rotation_zy.clear_points()
	p_rotation_zz.clear_points()
	p_scale_x.clear_points()
	p_scale_y.clear_points()
	p_scale_z.clear_points()
	
	position_x = p_position_x
	position_y = p_position_y
	position_z = p_position_z
	rotation_xx = p_rotation_xx
	rotation_xy = p_rotation_xy
	rotation_xz = p_rotation_xz
	rotation_yx = p_rotation_yx
	rotation_yy = p_rotation_yy
	rotation_yz = p_rotation_yz
	rotation_zx = p_rotation_zx
	rotation_zy = p_rotation_zy
	rotation_zz = p_rotation_zz
	scale_x = p_scale_x
	scale_y = p_scale_y
	scale_z = p_scale_z

func get_root_transform(in_t : float) -> Transform3D:
	if !rotation_xx:
		return Transform3D.IDENTITY
	var pos_x := position_x.sample(in_t)
	var pos_y := position_y.sample(in_t)
	var pos_z := position_z.sample(in_t)
	
	var rot_xx := rotation_xx.sample(in_t)
	var rot_xy := rotation_xy.sample(in_t)
	var rot_xz := rotation_xz.sample(in_t)
	var rot_yx := rotation_yx.sample(in_t)
	var rot_yy := rotation_yy.sample(in_t)
	var rot_yz := rotation_yz.sample(in_t)
	var rot_zx := rotation_zx.sample(in_t)
	var rot_zy := rotation_zy.sample(in_t)
	var rot_zz := rotation_zz.sample(in_t)
	var rot := Basis(Vector3(rot_xx, rot_xy, rot_xz), Vector3(rot_yx, rot_yy, rot_yz), Vector3(rot_zx, rot_zy, rot_zz))
	
	var sca_x := scale_x.sample(in_t)
	var sca_y := scale_y.sample(in_t)
	var sca_z := scale_z.sample(in_t)
	var out_transform = Transform3D(rot, Vector3(pos_x, pos_y, pos_z)).scaled_local(Vector3(sca_x, sca_y, sca_z))
	
	for i in global_transformations.size():
		out_transform = global_transformations[i].get_root_transform(in_t) * out_transform
	
	return out_transform
	
