@tool

class_name RoadShapePipeOpen extends RoadShape

@export var openness : Curve = Curve.new()

func find_t_from_relative_pos(in_pos : Vector3) -> Vector2:
	var tx : float = Vector2(in_pos.x, in_pos.y).normalized().angle() / PI
	tx /= maxf(0.001, openness.sample(in_pos.z))
	return Vector2(tx, in_pos.z)

func get_position_at_time(in_matrix : CurveMatrix, in_t : Vector2) -> Vector3:
	var road_transform_at_time := in_matrix.get_root_transform(in_t.y)
	
	var mod_t := (in_t.x + 1.0) * 0.5
	
	in_t.x *= openness.sample(in_t.y)
	
	var mod_vertical_offset := 0.0
	
	for mod in modulation_table:
		var mod_affector := mod.modulation_effect.sample(in_t.y)
		var mod_pos := mod.modulation_width.sample(1.0 - mod_t) * mod_affector
		#mod_transform.origin = mod_pos
		mod_vertical_offset += mod_pos
	
	var pos := Vector3(cos((in_t.x - 0.5) * PI), sin((in_t.x - 0.5) * PI), 0)
	var dir = pos.normalized()
	var road_transform : Transform3D = Transform3D(Basis.IDENTITY, pos + dir * mod_vertical_offset)
	#print(in_t)
	#print(road_transform.origin)
	#print("---")
	#road_transform.basis = mod_basis * road_transform.basis
	var final_transform : Transform3D = road_transform_at_time * road_transform
	return final_transform.origin

func get_surface_at_time(in_matrix : CurveMatrix, in_t : Vector2) -> PackedVector3Array:
	var out_array : PackedVector3Array = [Vector3.ZERO, Vector3.ZERO]
	out_array[0] = get_position_at_time(in_matrix, in_t)
	var arrow_color := Color(1, 0, 0)
	if in_t.x > 0:
		if in_t.y < 0.5:
			#print("case 1")
			var pos_right := get_position_at_time(in_matrix, in_t - Vector2(0.001, 0)) - out_array[0]
			var pos_forward := get_position_at_time(in_matrix, in_t + Vector2(0, 0.001)) - out_array[0]
			out_array[1] = pos_right.cross(pos_forward).normalized()
		else:
			#print("case 2")
			var pos_right := get_position_at_time(in_matrix, in_t - Vector2(0.001, 0)) - out_array[0]
			var pos_forward := get_position_at_time(in_matrix, in_t - Vector2(0, 0.001)) - out_array[0]
			out_array[1] = -pos_right.cross(pos_forward).normalized()
			arrow_color = Color(0, 1, 0)
	else:
		if in_t.y < 0.5:
			#print("case 3")
			var pos_right := get_position_at_time(in_matrix, in_t + Vector2(0.001, 0)) - out_array[0]
			var pos_forward := get_position_at_time(in_matrix, in_t + Vector2(0, 0.001)) - out_array[0]
			out_array[1] = -pos_right.cross(pos_forward).normalized()
			arrow_color = Color(0, 0, 1)
		else:
			#print("case 4")
			var pos_right := get_position_at_time(in_matrix, in_t + Vector2(0.001, 0)) - out_array[0]
			var pos_forward := get_position_at_time(in_matrix, in_t - Vector2(0, 0.001)) - out_array[0]
			out_array[1] = pos_right.cross(pos_forward).normalized()
			arrow_color = Color(1, 1, 1)
	#if out_array[1].is_zero_approx():
		#print(in_t)
	#print(out_array)
	#DebugDraw3D.draw_sphere(out_array[0], 0.2, arrow_color, 5.0)
	#DebugDraw3D.draw_arrow(out_array[0], out_array[0] + out_array[1] * 5, arrow_color, 2, true, 5.0)
	return out_array
