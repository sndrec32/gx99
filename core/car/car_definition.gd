class_name CarDefinition extends Resource

@export var model : Mesh = BoxMesh.new()

# higher = slower turning, tighter grip, affects air physics, higher max speed...
@export var weight : float = 1260

# overall linear increase to boosted and unboosted max speed
@export var max_speed : float = 0.172

# how quickly the machine reaches its target speed
@export var accel : float = 0.315

# length of boost
@export var boost_duration : float = 1.5

# energy usage for 1 boost
@export var boost_energy : float = 22

# max speed improvement when boosting
@export var boost_max_speed_mult : float = 1.6

# acceleration improvement when boosting
@export var boost_accel_mult : float = 1.4

# turbo increases max speed, added when boosting
@export var turbo_add : float = 32

# amt of turbo to deplete every second while boosting
@export var turbo_depletion_boosting : float = 16

# amt of turbo to deplete every second while not boosting
@export var turbo_depletion : float = 32

# strength of strafing
@export var strafe_power : float = 35

# add to turn movement when strafe turning
@export var strafe_turn_effect : float = 20

# multiplier to strafe strength when drifting and strafing in the same direction as your turn
@export var strafe_multiplier_quickturn : float = 0.35 

# multiplier to strafe strength when drifting and strafing in the opposite direction as your turn
@export var strafe_multiplier_mts : float = 1.25

# add to strafing when just turning
@export var turn_strafe_effect : float = 10

# how quickly steer input approaches maximum
@export var steer_acceleration : float = 0.12

# how fast the car wants to turn when holding the stick maximally left/right
@export var steer_speed_target : float = 40.0

# above, but for drift state
@export var steer_speed_target_drift : float = 101.5

# stick acceleration add to turning
@export var steer_reaction : float = 10

# relaxing effect on steer reaction
@export var steer_reaction_damp : float = 10

# machine wobbliness
@export var angle_drag : float = 20

# machine wobbliness when drifting
@export var angle_drag_drift : float = 6

# affects when machine loses grip from turning
@export var grip : float = 0.52875

# constant rate at which velocity moves towards nose of the car
@export var velocity_redirect_rate_constant : float = 0.7

# X axis is how far sideways the car is pointing, Y axis is strength of velocity redirect effect
@export var velocity_redirect_rate_proportional_curve : Curve = Curve.new()

# max speed in km/h for const redirect rate
@export var velocity_redirect_rate_constant_speed_max : float = 2500.0
# X axis is speed, Y axis is multiplier on constant redirect rate
@export var velocity_redirect_rate_constant_speed_threshold : Curve = Curve.new()

# max speed in km/h for prop redirect rate
@export var velocity_redirect_rate_proportional_speed_max : float = 2500.0
# X axis is speed in km/h, Y axis is multiplier on proportional redirect rate
@export var velocity_redirect_rate_proportional_speed_threshold : Curve = Curve.new()

# multiplier to velocity redirection when strafe dir matches steer dir
@export var velocity_redirect_quickturn_mult : float = 2.0

# multiplier to velocity redirection when strafe dir doesn't match steer dir
@export var velocity_redirect_turboslide_mult : float = 0.5

# speed gain/loss when drifting
@export var drift_accel : float = 0.4

# speed gain/loss when turning
@export var turn_accel : float = 0.013

# constant speed loss
@export var drag : float = 0.01

# health/boost energy
@export var health : float = 115

# energy to recharge per second when over a recharge strip
@export var health_recharge_rate : float = 40

@export var camera_turn_rate : float = 1.0
@export var camera_orientation_rate : float = 1.0
@export var car_collision_points : Array[Vector3] = []
@export var car_suspension_points : Array[CarSuspensionPoint] = []
