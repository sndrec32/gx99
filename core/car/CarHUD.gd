extends Control

@onready var health_meter = $HealthMeter
@onready var car := $".." as AGCar
@onready var speedometer = $Speedometer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	health_meter.scale.x = car.car_definition.health * 0.01
	var health_meter_shader := health_meter.material as ShaderMaterial
	health_meter_shader.set_shader_parameter("health_amount", car.health)
	health_meter_shader.set_shader_parameter("max_health_amount", car.car_definition.health)
	health_meter_shader.set_shader_parameter("can_boost", true)
	var boost_health_total_cost : float = car.car_definition.boost_energy * (1.0 / car.car_definition.boost_duration) * car.boost_time
	health_meter_shader.set_shader_parameter("health_to_deplete", boost_health_total_cost)
	var speed := (car.car_position - car.car_position_old) * FZGlobal.units_to_kmh
	speedometer.text = str(roundf(speed.length())) + " km/h"
