@tool

class_name AGCar extends Node3D

var current_checkpoint : int = 0
var car_position : Vector3 = Vector3.ZERO
var car_position_old : Vector3 = Vector3.ZERO
var air_velocity : Vector3 = Vector3.ZERO
var knockback_velocity : Vector3 = Vector3.ZERO
var base_speed : float = 0
var travel_direction : Vector3 = Vector3.FORWARD
var angle_velocity : Vector3 = Vector3.ZERO
var gravity_velocity : Vector3 = Vector3.ZERO
var orientation : Quaternion = Quaternion.IDENTITY
var gravity_orientation : Quaternion = Quaternion.IDENTITY
var health : float = 100.0
var drift_dot : float = 0
var steer_state : int = 0
var boost_time : float = 0
var boostpad_time : float = 0
var turbo : float = 0
var grounded : bool = true
var yaw_from_steering : float = 0
var roll_from_collision_and_strafe : float = 0
var pitch_from_speed : float = 0
var car_height : float = 0.0
var air_tilt : float = 0
var air_roll : float = 0
var time_in_grounded_state : float = 0
var turn_reaction_effect : float = 0
var prev_steer_horizontal : float = 0
var current_turning : float = 0
var checkpoints_to_test : PackedInt32Array = []
var suspension_points : Array[CarSuspensionPoint] = []
var cur_input : Array = []
var prev_input : Array = []
var camera_basis := Basis.IDENTITY

@export var car_definition : CarDefinition
@onready var camera_3d := $Camera3D as Camera3D
@onready var car_mesh = $CarVisual/CarMesh
@onready var car_visual = $CarVisual

var gravity_basis_slerped := Basis.IDENTITY

func _process(delta):
	if Engine.is_editor_hint():
		car_mesh.mesh = car_definition.model
		#DebugDraw3D.draw_position(car_visual.global_transform, Color(1, 1, 1), delta)
		#car_mesh.position = Vector3(0, 0.0, 0)
		camera_3d.position = Vector3(0, 4.5, 6.0)
		camera_3d.rotation_degrees = Vector3(-15, 0, 0)
		DebugDraw3D.scoped_config().set_thickness(0.01)
		for i in car_definition.car_collision_points.size():
			var point := car_definition.car_collision_points[i]
			DebugDraw3D.draw_sphere(point, 0.1, Color(1, 0, 0), delta)
		for i in car_definition.car_suspension_points.size():
			var point := car_definition.car_suspension_points[i]
			DebugDraw3D.draw_sphere(point.origin, 0.1, Color(0, 0, 1), delta)
			DebugDraw3D.draw_sphere(point.origin + point.target_dir * point.max_length, 0.1, Color(0, 0, 1), delta)
			DebugDraw3D.draw_line(point.origin, point.origin + point.target_dir * point.max_length, Color(0, 0, 1), delta)
		return
	var start_offset = 1.0 - clampf(remap(car_tick, 15, 45, 0, 1), 0, 1)
	car_visual.position = car_position + car_visual.basis.y * (start_offset * -2.8 + car_height)
	car_visual.quaternion = orientation
	car_visual.basis = car_visual.basis.rotated(car_visual.basis.y, yaw_from_steering)
	car_visual.basis = car_visual.basis.orthonormalized()
	var car_basis := Basis(orientation)
	
	var gravity_basis = Basis(gravity_orientation)
	gravity_basis_slerped = gravity_basis_slerped.slerp(gravity_basis, delta * 8)
	if grounded:
		var camera_basis_z := travel_direction.slide(gravity_basis_slerped.y).normalized()
		var camera_basis_x := camera_basis_z.cross(gravity_basis_slerped.y).normalized()
		var camera_basis_y := camera_basis_z.cross(camera_basis_x).normalized()
		#print(camera_basis_x)
		#print(camera_basis_y)
		#print(camera_basis_z)
		
		var target_camera_basis := Basis(-camera_basis_x, -camera_basis_y, camera_basis_z).orthonormalized()
		target_camera_basis = target_camera_basis * Basis.from_euler(Vector3(0, deg_to_rad(180), 0))
		#print(camera_basis.is_conformal())
		camera_basis = camera_basis.slerp(target_camera_basis, delta * car_definition.camera_turn_rate).orthonormalized()
	else:
		camera_basis = camera_basis.slerp(car_basis.rotated(car_basis.z, air_tilt * current_turning * -0.01), delta * car_definition.camera_turn_rate * 0.25).orthonormalized()
		
	camera_3d.position = car_position + camera_basis.z * 6 + camera_basis.y * 5
	camera_3d.basis = camera_basis * Basis.from_euler(Vector3(deg_to_rad(-20), 0, 0))
	
	var track_segments := get_tree().get_nodes_in_group("TrackRoot")[0] as TrackRoot
	var cp := track_segments.checkpoints[current_checkpoint]
	var render_basis := cp.orientation_start.rotated(cp.orientation_start.x, PI * 0.5)
	render_basis.x *= cp.x_radius_start
	render_basis.z *= cp.y_radius_start
	render_basis.y = render_basis.y.normalized() * 0.25
	#DebugDraw3D.draw_plane(cp.start_plane, Color(0, 1, 0, 0.05), cp.position_start, delta)
	render_basis = cp.orientation_end.rotated(cp.orientation_end.x, PI * 0.5)
	render_basis.x *= cp.x_radius_end
	render_basis.z *= cp.y_radius_end
	render_basis.y = render_basis.y.normalized() * 0.25
	#DebugDraw3D.draw_plane(cp.end_plane, Color(1, 0, 0, 0.05), cp.position_end, delta)

func _ready():
	update_checkpoints()
	camera_3d.make_current()
	suspension_points = car_definition.car_suspension_points.duplicate(true)
	health = car_definition.health

func draw_checkpoint(in_checkpoint : Checkpoint):
	var render_basis := in_checkpoint.orientation_start.rotated(in_checkpoint.orientation_start.x, PI * 0.5)
	render_basis.x *= in_checkpoint.x_radius_start
	render_basis.z *= in_checkpoint.y_radius_start
	render_basis.y = render_basis.y.normalized() * 0.25
	DebugDraw3D.draw_cylinder(Transform3D(render_basis, in_checkpoint.position_start + render_basis.y * 0.5), Color(0, 1, 0, 0.1), 0.016666)
	#DebugDraw3D.draw_plane(Plane(in_checkpoint.orientation_start.z, in_checkpoint.position_start), Color(0, 1, 0, 0.05), in_checkpoint.position_start, 0.016666)
	render_basis = in_checkpoint.orientation_end.rotated(in_checkpoint.orientation_end.x, PI * 0.5)
	render_basis.x *= in_checkpoint.x_radius_end
	render_basis.z *= in_checkpoint.y_radius_end
	render_basis.y = render_basis.y.normalized() * 0.25
	DebugDraw3D.draw_cylinder(Transform3D(render_basis, in_checkpoint.position_end - render_basis.y * 0.5), Color(1, 0, 0, 0.1), 0.016666)

func get_road_data() -> Basis:
	update_checkpoints()
	var track_segments := get_tree().get_nodes_in_group("TrackRoot")[0] as TrackRoot
	var segment := track_segments.checkpoints[current_checkpoint].road_segment
	var actual_cp := track_segments.checkpoints[current_checkpoint] as Checkpoint
	draw_checkpoint(actual_cp)
	var cp_plane_start : Plane = Plane(actual_cp.orientation_start.z, actual_cp.position_start)
	var cp_plane_end : Plane = Plane(actual_cp.orientation_end.z, actual_cp.position_end)
	var p1 := cp_plane_start.project(car_position)
	var p2 := cp_plane_end.project(car_position)
	var closestPointUncapped : Vector3 = Geometry3D.get_closest_point_to_segment_uncapped(car_position, p1, p2)
	#DebugDraw3D.draw_line(p1, p2, Color.WHITE, 0.01666)
	var l_dot := (closestPointUncapped - p1).dot((p2 - p1).normalized())
	var t := l_dot / (p2 - p1).length()
	var tz = remap(t, 0, 1, actual_cp.y_start, actual_cp.y_end)
	var use_curve := track_segments.get_child(segment) as RoadPath
	#var road_root := use_curve.road_curve.get_root_transform(Vector3(0, ty, 0))
	#var mid_point := road_root.origin
	#var basis_at_y := road_root.basis
	var mid_point := actual_cp.position_start.lerp(actual_cp.position_end, t)
	#DebugDraw3D.draw_line(actual_cp.position_start, actual_cp.position_end, Color.GREEN, 0.01666)
	var basis_at_y := actual_cp.orientation_start.slerp(actual_cp.orientation_end, t)
	var angle := (car_position - mid_point).normalized().signed_angle_to(basis_at_y.y.normalized(), basis_at_y.z.normalized())
	if angle < 0:
		angle = PI * 2 - absf(angle)
	var separating_plane_normal : Vector3 = basis_at_y.x
	var separating_plane : Plane = Plane(separating_plane_normal, mid_point)
	var separating_plane_normal_y : Vector3 = basis_at_y.y
	var separating_plane_y : Plane = Plane(separating_plane_normal_y, mid_point)
	#var skew := (p2 - p1).normalized().angle_to((actual_cp.position_end - actual_cp.position_start).normalized())
	#skew *= 0.55
	#print(tan(skew))
	#DebugDraw3D.draw_plane(separating_plane, Color(1.0, 0.4, 0.4, 0.2), mid_point, 0.01666)
	var x_radius_at_t := lerpf(actual_cp.x_radius_start, actual_cp.x_radius_end, t)
	var y_radius_at_t := lerpf(actual_cp.y_radius_start, actual_cp.y_radius_end, t)
	#radius_at_y *= (1.0 - skew)
	var tx = separating_plane.distance_to(car_position) / x_radius_at_t
	var ty = separating_plane_y.distance_to(car_position) / y_radius_at_t
	print("---")
	print(tx)
	print(ty)
	#DebugDraw3D.draw_position(Transform3D(basis_at_y.scaled(Vector3.ONE * 4), car_position), Color.RED, 0.016666)
	#DebugDraw3D.draw_line(separating_plane.project(car_position), car_position, Color.CYAN, 0.016666)
	#print("----")
	#print(tx)
	#print(ty)
	#if tx < -1.0 or tx > 1.0 or tz < 0.0 or tz > 1.0 or ty < -1.0 or ty > 1.0:
	#	return Basis.IDENTITY
	#var skew_angle := (p2 - p1).normalized().angle_to(basis_at_y.z)
	#print(skew_angle)
	#print(sin(skew_angle))
	#print(tan(skew_angle))
	#tx *= 1.0 - sin(skew_angle)
	#var leftmost := use_curve.get_cached_surface_fast_indexed_ugly_basis_tweak_direct(-0.999, ty, 0)
	#var rightmost := use_curve.get_cached_surface_fast_indexed_ugly_basis_tweak_direct(0.999, ty, 0)
	#DebugDraw3D.draw_line(leftmost.x - Vector3(0, 0.1, 0), rightmost.x - Vector3(0, 0.1, 0), Color.YELLOW, 0.01666)
	#var surface := use_curve.get_cached_surface_fast_indexed_ugly_basis_tweak_direct(tx, ty, angle)
	var surface := use_curve._get_surface(use_curve.road_shape.find_t_from_relative_pos(Vector3(tx, ty, tz)))
	#DebugDraw3D.draw_sphere(surface.x, 0.5, Color.AQUA, 0.0166)
	#DebugDraw3D.draw_arrow(surface.x, surface.x + surface.y * 4, Color.AQUA, 0.01666)
	
	return surface

func calculate_top_speed() -> float:
	var base_max_speed := 900.0 + (1.0 + (car_definition.weight * 0.01)) + car_definition.max_speed * 2000 + turbo * 50
	if boost_time > 0.0:
		base_max_speed = base_max_speed * car_definition.boost_max_speed_mult
	if boostpad_time > 0.0:
		base_max_speed = base_max_speed * car_definition.boost_max_speed_mult
	return FZGlobal.kmh_to_units * base_max_speed

func calculate_acceleration(in_speed : float) -> float:
	var top_speed := calculate_top_speed()
	var base_accel = lerpf(car_definition.accel * 8, 0, remap(in_speed, 0, top_speed, 0, 1)) * FZGlobal.tick_delta
	if boostpad_time > 0.0 or boost_time > 0.0:
		base_accel *= car_definition.boost_accel_mult
	return base_accel

func _physics_process(delta):
	if Engine.is_editor_hint():
		return
	var time_before := Time.get_ticks_usec()
	tick("blah")
	#print(base_speed * FZGlobal.units_to_kmh)
	car_tick += 1
	RenderingServer.global_shader_parameter_set("race_time_elapsed", float(car_tick) / Engine.physics_ticks_per_second)
	var time_after := Time.get_ticks_usec()
	var time_elapsed_ms := 0.001 * (time_after - time_before)
	#print(time_elapsed_ms * 1000)

func get_all_viable_checkpoints() -> PackedInt32Array:
	var track_segments := get_tree().get_nodes_in_group("TrackRoot")[0] as TrackRoot
	assert(track_segments.checkpoints.size() > 0)
	var checkpoint_array : PackedInt32Array = []
	for i in track_segments.checkpoints.size():
		#print("testing checkpoint " + str(i))
		var cp = track_segments.checkpoints[i]
		var end_plane : Plane = Plane(cp.orientation_end.z, cp.position_end)
		var start_plane : Plane = Plane(cp.orientation_start.z, cp.position_start)
		if end_plane.distance_to(car_position) <= 0 and start_plane.distance_to(car_position) >= 0:
			#print("inside't!")
			checkpoint_array.append(i)
		if end_plane.distance_to(car_position) > 0 and start_plane.distance_to(car_position) < 0:
			#print("inside't weirdly!")
			checkpoint_array.append(i)
	return checkpoint_array

func update_checkpoints() -> void:
	var track_segments := get_tree().get_nodes_in_group("TrackRoot")[0] as TrackRoot
	assert(track_segments.checkpoints.size() > 0)
	for i in 9:
		var prev_cp_index := current_checkpoint - 1
		if prev_cp_index < 0:
			prev_cp_index = track_segments.checkpoints.size() - 1
		var next_cp_index := current_checkpoint + 1
		if next_cp_index >= track_segments.checkpoints.size():
			next_cp_index = 0
		var cp = track_segments.checkpoints[prev_cp_index]
		var cp_2 = track_segments.checkpoints[next_cp_index]
		if cp.end_plane.distance_to(car_position) <= 0:
			current_checkpoint = prev_cp_index
			#print("exited checkpoint " + str(current_checkpoint))
			#print("entered checkpoint " + str(prev_cp_index))
			#print("should update checkpoints to test")
		elif cp_2.start_plane.distance_to(car_position) >= 0:
			current_checkpoint = next_cp_index
			#print("exited checkpoint " + str(current_checkpoint))
			#print("entered checkpoint " + str(prev_cp_index))
			#print("should update checkpoints to test")
		else:
			#print("no update, breaking at iteration " + str(i))
			return

# Called every frame. 'delta' is the elapsed time since the previous frame.
var car_tick := 0

func tick(in_inputs):
	if car_tick <= 60:
		update_checkpoints()
		return
	var gravity_basis := Basis(gravity_orientation)
	var car_basis := Basis(orientation)
	var use_z := car_basis.z.slide(gravity_basis.y).normalized()
	var use_x := car_basis.x.slide(gravity_basis.y).normalized()
	
	var input_strafe := Input.get_axis("StrafeLeft", "StrafeRight")
	var input_strafe_left := Input.get_action_strength("StrafeLeft")
	var input_strafe_right := Input.get_action_strength("StrafeRight")
	var input_steer_horizontal := Input.get_axis("SteerRight", "SteerLeft")
	var input_steer_vertical := Input.get_axis("SteerUp", "SteerDown")
	var input_accelerate := Input.get_action_strength("Accelerate")
	var input_brake := Input.get_action_strength("Brake")
	var input_spinattack := Input.is_action_just_pressed("SpinAttack")
	var input_boost := Input.is_action_just_pressed("Boost")
	
	input_steer_horizontal = pow(absf(input_steer_horizontal), 2) * signf(input_steer_horizontal)
	
	if cur_input.size() == 0:
		prev_input = [0.0, 0.0, 0.0, 0.0, 0.0, false, false]
	else:
		prev_input = cur_input
	cur_input = [input_steer_horizontal, input_steer_vertical, input_strafe, input_accelerate, input_brake, input_boost, input_spinattack]
	var car_movement := (car_position - car_position_old) / FZGlobal.tick_delta # vehicle movement expressed in units per second
	var car_apparent_speed := car_movement.length()
	var car_speed_kmh := car_apparent_speed * FZGlobal.units_to_kmh * FZGlobal.tick_delta
	
	# // HANDLE BOOST STATES // #
	
	if input_boost and health > 1.0 and boost_time == 0.0 and boostpad_time == 0.0:
		boost_time = car_definition.boost_duration
		turbo += car_definition.turbo_add
	
	if boost_time > 0.0 or boostpad_time > 0.0:
		if boost_time > 0.0:
			health = maxf(1.0, health - (car_definition.boost_energy * FZGlobal.tick_delta) / car_definition.boost_duration)
		turbo = maxf(0.0, turbo - car_definition.turbo_depletion_boosting)
	else:
		turbo = maxf(0.0, turbo - car_definition.turbo_depletion)
	
	if health <= 1.0:
		boost_time = 0.0
	
	boost_time = maxf(0.0, boost_time - FZGlobal.tick_delta)
	boostpad_time = maxf(0.0, boostpad_time - FZGlobal.tick_delta)
	
	# // HANDLE STEER STATES // #
	
	var angle_from_vel_dir : float = travel_direction.slide(gravity_basis.y).normalized().signed_angle_to(-use_z, gravity_basis.y) # 
	angle_from_vel_dir = (angle_from_vel_dir / PI) * 2.0
	
	if !grounded:
		steer_state = 0
	else:
		var drift_magic := car_apparent_speed * (absf(current_turning) + absf(angle_from_vel_dir * 1000))
		if drift_magic >= 20000 or (input_strafe_left > 0.05 and input_strafe_right > 0.05):
			steer_state = 1
		else:
			if sign(input_strafe) != sign(current_turning):
				steer_state = 0
		#DebugDraw2D.set_text("Dir Offset", angle_from_vel_dir)
		#DebugDraw2D.set_text("Drift Magic", drift_magic)
	#steer_state
	
	# // HANDLE STRAFING // #
	
	var strafe_add := use_x * input_strafe * car_apparent_speed * car_definition.strafe_power * 0.015
	var strafe_steer_parity := input_strafe * signf(angle_from_vel_dir)
	if steer_state == 1 and !is_zero_approx(input_strafe):
		if strafe_steer_parity < 0:
			strafe_add *= lerpf(1.0, car_definition.strafe_multiplier_quickturn, absf(strafe_steer_parity))
		else:
			strafe_add *= lerpf(1.0, car_definition.strafe_multiplier_mts, absf(strafe_steer_parity))
			strafe_add = strafe_add.lerp(strafe_add.project(travel_direction), 0.5)
			
	
	strafe_add += use_x * input_steer_horizontal * car_apparent_speed * -0.25
	# // HANDLE STEERING CONTROLS // #
	
	var use_target := car_definition.steer_speed_target
	if steer_state == 1:
		use_target = car_definition.steer_speed_target_drift
	current_turning = lerpf(current_turning, use_target * cur_input[0], car_definition.steer_acceleration * FZGlobal.tick_delta * 120)
	turn_reaction_effect += (cur_input[0] - prev_input[0]) * car_definition.steer_reaction * 3.5
	turn_reaction_effect += -turn_reaction_effect * FZGlobal.tick_delta * 11 * car_definition.steer_reaction_damp
	var total_turning = (current_turning + turn_reaction_effect) / (1.0 + car_definition.weight * 0.002)
	yaw_from_steering = lerpf(yaw_from_steering, total_turning * 0.01, FZGlobal.tick_delta * 6)
	angle_velocity += gravity_basis.y * total_turning * FZGlobal.tick_delta * 4
	angle_velocity += -angle_velocity * car_definition.angle_drag * FZGlobal.tick_delta
	
	if grounded:
		var angle_to_travel_dir := (travel_direction.angle_to(-use_z) / PI) * 2.0
		#print(angle_to_travel_dir)
		var drift_angle_adjust := 0.01
		if steer_state == 1:
			drift_angle_adjust = 0.1
		var redir_const_const = car_definition.velocity_redirect_rate_constant * car_definition.velocity_redirect_rate_constant_speed_threshold.sample_baked(car_speed_kmh / car_definition.velocity_redirect_rate_constant_speed_max)
		var redir_const = 1.0 / maxf(drift_angle_adjust, angle_to_travel_dir) * redir_const_const
		var redir_prop_const = car_definition.velocity_redirect_rate_proportional_curve.sample_baked(absf(angle_to_travel_dir))
		var redir_prop = redir_prop_const * car_definition.velocity_redirect_rate_proportional_speed_threshold.sample_baked(car_speed_kmh / car_definition.velocity_redirect_rate_proportional_speed_max)
		#print("----")
		#print(car_speed_kmh)
		#print(redir_const)
		#print(redir_prop)
		#var redir_prop_affector := 1.0 - (angle_to_travel_dir * car_definition.velocity_redirect_rate_proportional_snap) / (1.0 + base_speed * 0.1)
		#redir_prop = lerpf(redir_prop, 0.0, redir_prop_affector)
		#print(redir_prop)
		#print(strafe_steer_parity)
		if strafe_steer_parity < 0:
			redir_const = lerpf(redir_const, redir_const * car_definition.velocity_redirect_quickturn_mult, absf(input_strafe))
		if strafe_steer_parity > 0:
			redir_const = lerpf(redir_const, redir_const * car_definition.velocity_redirect_turboslide_mult, absf(input_strafe))
		#print(redir_prop)
		var redir_total = minf(1.0, (redir_const + redir_prop) * FZGlobal.tick_delta)
		#print(redir_total)
		travel_direction = travel_direction.slerp(-use_z, redir_total).normalized()
	else:
		travel_direction = travel_direction.slerp(-use_z, FZGlobal.tick_delta * 20).normalized()
	
	
	# // HANDLE VEHICLE ORIENTATION AND ANGULAR VELOCITY // #
	
	car_position_old = car_position
	
	if !grounded:
		air_tilt = clampf(air_tilt - input_steer_vertical * FZGlobal.tick_delta * 4, -1, 1)
		var desired_use_z = use_z.rotated(use_x, air_tilt)
		var signed_angle = clampf(car_basis.z.signed_angle_to(desired_use_z, use_x), -0.1, 0.1)
		#print(signed_angle)
		angle_velocity += signed_angle * use_x * FZGlobal.tick_delta * 1200
	
	#print(air_tilt)
	
	if !angle_velocity.is_zero_approx():
		orientation = Quaternion(angle_velocity.normalized(), angle_velocity.length() * FZGlobal.tick_delta) * orientation
	orientation = orientation.normalized()
	base_speed += -base_speed * FZGlobal.tick_delta * car_definition.drag * 10
	base_speed = move_toward(base_speed, 0, car_definition.drag * FZGlobal.tick_delta * 10)
	base_speed += calculate_acceleration(car_apparent_speed * FZGlobal.tick_delta) * input_accelerate
	var total_movement := travel_direction * base_speed * 60 + knockback_velocity + strafe_add
	var just_landed_vel := Vector3.ZERO
	if !grounded:
		if air_velocity.is_zero_approx():
			for point in suspension_points:
				point.current_force == Vector3.ZERO
		air_velocity += gravity_basis.y * -2 * FZGlobal.tick_delta
		total_movement += air_velocity
	else:
		air_tilt = 0
		just_landed_vel = air_velocity
		air_velocity = Vector3.ZERO
	car_position += total_movement * FZGlobal.tick_delta
	var surface_data := get_road_data()
	var surface_position := surface_data.x
	var surface_normal := surface_data.y
	var tx := surface_data.z.x
	var ty := surface_data.z.y
	var tz := surface_data.z.z
	var to_gravity := Quaternion(gravity_basis.y, surface_normal)
	var car_transform := Transform3D(car_basis, car_position)
	if surface_data == Basis.IDENTITY:
		grounded = false
	else:
		
		if absf(tx) < 1.0:
			if grounded:
				#to_gravity = Quaternion(gravity_basis.y, surface_normal)
				var signed_dist := (car_position - surface_position).dot(surface_normal)
				#print(signed_dist)
				car_position += surface_normal * -signed_dist
				#DebugDraw3D.scoped_config().set_thickness(0.03)
				var all_airborne : bool = true
				#car_position += -surface_normal * car_definition.weight * FZGlobal.tick_delta * 0.03
				var i := 0
				var force_sum := Vector3.ZERO
				for point in suspension_points:
					var global_point_origin : Vector3 = car_transform * (point.origin)
					var global_point_target : Vector3 = car_transform * (point.origin + point.target_dir * point.max_length)
					#DebugDraw3D.draw_sphere(global_point_origin, 0.1, Color(0, 0, 1), FZGlobal.tick_delta * 0.5)
					#DebugDraw3D.draw_sphere(global_point_target, 0.1, Color(0, 0, 1), FZGlobal.tick_delta * 0.5)
					var about_surface := global_point_target - surface_position
					var about_car := global_point_origin - car_position
					var depth := (about_surface).dot(surface_normal)
					#DebugDraw3D.draw_sphere(global_point_target + surface_normal * -depth, 0.05, Color.YELLOW, FZGlobal.tick_delta * 0.5)
					var movement_plus_gravity := (total_movement + surface_normal * -60 + just_landed_vel) * FZGlobal.tick_delta
					var surface_force := movement_plus_gravity.dot(surface_normal) * car_definition.weight * FZGlobal.tick_delta * 0.25
					point.current_force += -surface_normal * surface_force
					point.current_force += -point.current_force * FZGlobal.tick_delta * 15
					force_sum += point.current_force
					#DebugDraw2D.set_text(str(i), point.current_force)
					#DebugDraw3D.draw_arrow(global_point_origin, global_point_origin + point.current_force * 0.1, Color.RED, 0.2, true, FZGlobal.tick_delta * 0.5)
					#car_position += -surface_normal * surface_force
					#print(depth)
					#if point.current_force.length() >= 0.1:
					#	all_airborne = false
					if depth < 0:
						#var true_depth := depth - 0.3
						#print(depth)
						var reorient_strength := point.spring_strength
						var angle_vel_add := about_car.cross(-surface_normal) * depth * reorient_strength
						#DebugDraw3D.draw_arrow(global_point_origin, global_point_origin + angle_vel_add, Color(1, 0, 0), 0.2, true, FZGlobal.tick_delta * 0.5)
						angle_velocity += angle_vel_add * 120 * FZGlobal.tick_delta
						all_airborne = false
					i += 1
				
				#print(force_sum.length() * 0.01)
				car_height = maxf(0.0, 1 - force_sum.length() * 0.01)
				#print(car_height)
				
				if all_airborne:
					grounded = false
				#todo: calculate change in slope over movement to figure out if the vehicle should lift off
			else:
				var signed_dist := (car_position - surface_position).dot(surface_normal)
				var signed_dist_2 := (car_position_old - surface_position).dot(surface_normal)
				if signed_dist <= 0 and signed_dist_2 >= 0:
					car_position += surface_normal * -signed_dist
					#velocity += surface_normal * surface_normal.dot(velocity) * -1.5
					grounded = true
					to_gravity = Quaternion(gravity_basis.y, surface_normal)
				if signed_dist >= -0.1 and signed_dist_2 <= -0.1:
					car_position += surface_normal * (signed_dist + 0.1)
					print("hitting bottom of road!")
				if !grounded and signed_dist > 2:
					to_gravity = Quaternion(gravity_basis.y, Vector3.UP)
	
	gravity_velocity += to_gravity.get_axis() * to_gravity.get_angle() * 8 * FZGlobal.tick_delta
	gravity_velocity += -gravity_velocity * FZGlobal.tick_delta * 12
	
	if !gravity_velocity.is_zero_approx() and grounded:
		var gravity_velocity_add := Quaternion(gravity_velocity.normalized(), gravity_velocity.length())
		gravity_orientation = (gravity_velocity_add * gravity_orientation).normalized()
	elif to_gravity.get_axis().is_normalized():
		var gravity_orientation_change := Quaternion(to_gravity.get_axis(), to_gravity.get_angle() * FZGlobal.tick_delta * 8)
		gravity_orientation = gravity_orientation_change * gravity_orientation
		orientation = gravity_orientation_change * orientation
		#gravity_velocity = to_gravity.get_axis() * to_gravity.get_angle()
	
	# // DEBUG DRAW SECTION // #
	
	#DebugDraw3D.scoped_config().set_thickness(0.25)
	#DebugDraw3D.draw_arrow(car_position, car_position + travel_direction * 5, Color.YELLOW, 0.1, true, FZGlobal.tick_delta * 0.25)
	#DebugDraw3D.draw_arrow(car_position, car_position + -use_z * 7.5, Color.ORANGE, 0.1, true, FZGlobal.tick_delta * 0.25)
	
	#DebugDraw3D.scoped_config().set_thickness(0.1)
	#DebugDraw3D.draw_arrow(car_position, car_position + gravity_basis.x * 2.5, Color.RED, 0.1, true, FZGlobal.tick_delta * 0.25)
	#DebugDraw3D.draw_arrow(car_position, car_position + gravity_basis.y * 2.5, Color.GREEN, 0.1, true, FZGlobal.tick_delta * 0.25)
	#DebugDraw3D.draw_arrow(car_position, car_position + gravity_basis.z * 2.5, Color.BLUE, 0.1, true, FZGlobal.tick_delta * 0.25)
	
	#DebugDraw3D.draw_arrow(car_position, car_position + surface_normal * 2.5, Color.CYAN, 0.1, true, FZGlobal.tick_delta * 0.25)
	
	
	if Input.is_action_just_pressed("SpinAttack"):
		print("RESTART!")
		car_position = Vector3.ZERO
		angle_velocity = Vector3.ZERO
		base_speed = 0
		current_checkpoint = 0
		orientation = Quaternion.IDENTITY
		grounded = true
