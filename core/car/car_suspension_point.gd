class_name CarSuspensionPoint extends Resource

@export var origin : Vector3 = Vector3.ZERO
@export var target_dir : Vector3 = Vector3.ZERO
@export var spring_strength : float = 1.0
@export var spring_damp : float = 0.5
@export var max_length : float = 20

var current_force : Vector3 = Vector3.ZERO
