@tool

class_name RoadPathLine extends RoadPath

@export var start_pos : Vector3 = Vector3.ZERO
@export var end_pos : Vector3 = Vector3.ZERO
@export var start_scale : Vector3 = Vector3.ONE * 45
@export var end_scale : Vector3 = Vector3.ONE * 45
@export var start_rotation : Basis = Basis.IDENTITY
@export var end_rotation : Basis = Basis.IDENTITY
@export var start_tangent : float = 0.5
@export var end_tangent : float = 0.5

var start_marker : Marker3D
var end_marker : Marker3D
var should_update := false


static var line_plugin

class LinePathInspector extends EditorInspectorPlugin:
	func _can_handle(object): return object is RoadPathLine
	
	func _parse_begin(object):
		var cpc = object as RoadPathLine
		
		var button_2 = Button.new()
		button_2.text = "Refresh Mesh"
		button_2.pressed.connect(cpc._try_generate_mesh)
		
		var box_container = VBoxContainer.new()
		box_container.add_child(button_2)
		
		var container = MarginContainer.new()
		container.add_theme_constant_override("margin_bottom", 10)
		container.add_child(box_container)
		
		add_custom_control(container)

func _enter_tree():
	if Engine.is_editor_hint():
		if line_plugin == null:
			line_plugin = EditorPlugin.new()
			line_plugin.add_inspector_plugin(LinePathInspector.new())


func _ready():
	for child in get_children():
		if child is Marker3D:
			child.free()
	start_marker = Marker3D.new()
	add_child(start_marker)
	start_marker.owner = get_tree().get_edited_scene_root()
	start_marker.global_position = start_pos
	start_marker.global_basis = start_rotation
	start_marker.scale = start_scale
	start_marker.name = "Start"
	
	end_marker = Marker3D.new()
	add_child(end_marker)
	end_marker.owner = get_tree().get_edited_scene_root()
	if !end_pos.is_zero_approx():
		end_marker.global_position = end_pos
	else:
		end_marker.global_position = Vector3(0, 0, 250)
	end_marker.global_basis = end_rotation
	end_marker.scale = end_scale
	end_marker.name = "End"

var is_clicking := false
var click_timeout := 0

func _process(delta):
	if !(road_shape and road_curve):
		return
	if !(road_curve.position_x):
		return
	if !Engine.is_editor_hint():
		return
	
	var editor_interface := Engine.get_singleton("EditorInterface")
	
	
	var just_clicked := false
	
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_MIDDLE):
		if !is_clicking and Time.get_ticks_msec() > click_timeout + 0.5:
			just_clicked = true
			click_timeout = Time.get_ticks_msec()
		is_clicking = true
	else:
		is_clicking = false
	
	var editor_cam:Camera3D = editor_interface.get_editor_viewport_3d(0).get_camera_3d()
	for child in get_children():
		var node_colour := Color.WHITE
		var node_size := 2.5
		if child is Marker3D:
			var plane := Plane(editor_cam.global_basis.z, child.global_position)
			var intersect = plane.intersects_ray(editor_cam.global_position, editor_cam.project_ray_normal(editor_interface.get_editor_viewport_3d(0).get_mouse_position()))
			var can_be_clicked = false
			if intersect and intersect.distance_to(child.global_position) < 16:
				node_colour = Color.CYAN
				node_size = 5.0
				if just_clicked:
					if !Input.is_key_pressed(KEY_SHIFT):
						EditorInterface.get_selection().clear()
					EditorInterface.get_selection().add_node(child)
			
			if EditorInterface.get_selection().get_selected_nodes().find(child) != -1:
				node_colour = Color.RED
				node_size = 3.0
			DebugDraw3D.draw_sphere(child.global_position, node_size, node_colour, delta)
	
	if !(start_marker.global_position != start_pos):
		should_update = true
	if !(end_marker.global_position != end_pos):
		should_update = true
	if !(start_marker.global_basis.orthonormalized() != start_rotation):
		should_update = true
	if !(end_marker.global_basis.orthonormalized() != end_rotation):
		should_update = true
	if !(start_marker.scale != start_scale):
		should_update = true
	if !(end_marker.scale != end_scale):
		should_update = true
	
	if Time.get_ticks_msec() > last_gen_time + 100 and should_update:
		last_gen_time = Time.get_ticks_msec()
		road_curve.clear_all_keyframes()
		road_curve.key_pos_at_time(start_marker.global_position, 0)
		road_curve.key_pos_at_time(end_marker.global_position, 1)
		road_curve.key_rot_at_time(start_marker.global_basis.orthonormalized(), 0)
		road_curve.key_rot_at_time(end_marker.global_basis.orthonormalized(), 1)
		road_curve.key_scale_at_time(start_marker.scale, 0)
		road_curve.key_scale_at_time(end_marker.scale, 1)
		road_curve.make_all_keys_linear()
		start_pos = start_marker.global_position
		end_pos = end_marker.global_position
		start_rotation = start_marker.global_basis.orthonormalized()
		end_rotation = end_marker.global_basis.orthonormalized()
		start_scale = start_marker.scale
		end_scale = end_marker.scale
		segment_length = start_pos.distance_to(end_pos)
		_try_generate_mesh()
		should_update = false
		for gizmo:EditorNode3DGizmo in get_gizmos():
			gizmo._redraw()
