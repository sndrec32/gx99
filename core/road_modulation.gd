class_name RoadModulation extends Resource

@export var modulation_width : Curve
@export var modulation_effect : Curve
