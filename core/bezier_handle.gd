@tool

class_name BezierHandle extends Marker3D

@export var in_handle_length : float = 1.0
@export var out_handle_length : float = 1.0
@export var time : float = 0.5

var editor_interface:Object

func _ready() -> void:
	if Engine.is_editor_hint():
		editor_interface = Engine.get_singleton("EditorInterface")

var is_clicking := false
var click_timeout := 0

func _process(delta: float) -> void:
	var node_colour := Color.WHITE
	var node_size := 2.5
	
	
	if EditorInterface.get_selection().get_selected_nodes().find(self) != -1:
		node_colour = Color.RED
		node_size = 3.0
	
	DebugDraw3D.scoped_config().set_thickness(0.5)
	DebugDraw3D.scoped_config().set_no_depth_test(true)
	var p1 := global_position + global_basis.orthonormalized().z * out_handle_length
	var p2 := global_position + global_basis.orthonormalized().z * -in_handle_length
	DebugDraw3D.draw_sphere(global_position, node_size, node_colour, delta)
	DebugDraw3D.draw_sphere(p1, node_size * 0.5, node_colour, delta)
	DebugDraw3D.draw_sphere(p2, node_size * 0.5, node_colour, delta)
	DebugDraw3D.draw_line(global_position, p1, node_colour, delta)
	DebugDraw3D.draw_line(global_position, p2, node_colour, delta)
	
