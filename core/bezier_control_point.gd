@tool

class_name BezierControlPoint extends Resource

@export var index : int = 0
@export var position : Vector3 = Vector3.ZERO
@export var rotation : Basis = Basis.IDENTITY
@export var scale : Vector3 = Vector3.ZERO
@export var handle_in : float = 5
@export var handle_out : float = 5
@export var time : float = 0

var handle_in_pos : Vector3:
	set(in_pos):
		handle_in = in_pos.distance_to(position)
	get:
		return position + rotation.z * -handle_in

var handle_out_pos : Vector3:
	set(in_pos):
		handle_out = in_pos.distance_to(position)
	get:
		return position + rotation.z * handle_out
