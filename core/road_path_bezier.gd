@tool

class_name RoadPathBezier extends RoadPath

@export var bezier_points : Array[BezierControlPoint] = []

var bezier_handle_nodes : Array[BezierHandle] = []

static var bezier_plugin

class BezierPathInspector extends EditorInspectorPlugin:
	func _can_handle(object): return object is RoadPathBezier
	
	func _parse_begin(object):
		var cpc = object as RoadPathBezier
		
		var button = Button.new()
		button.text = "Refresh Curve"
		button.pressed.connect(cpc.calculate_curves_from_bezier)
		
		var button_2 = Button.new()
		button_2.text = "Refresh Mesh"
		button_2.pressed.connect(cpc._try_generate_mesh)
		
		var box_container = VBoxContainer.new()
		box_container.add_child(button)
		box_container.add_child(button_2)
		
		var container = MarginContainer.new()
		container.add_theme_constant_override("margin_bottom", 10)
		container.add_child(box_container)
		
		add_custom_control(container)

func _enter_tree():
	if Engine.is_editor_hint():
		if bezier_plugin == null:
			bezier_plugin = EditorPlugin.new()
			bezier_plugin.add_inspector_plugin(BezierPathInspector.new())

func sample_bezier_at_time(in_t : float) -> Transform3D:
	var use_bezier_index := 0
	for b in bezier_points.size():
		if bezier_points[b].time > in_t:
			use_bezier_index = b
			break
	var b_start := bezier_points[use_bezier_index - 1]
	var b_end := bezier_points[use_bezier_index]
	var b_t := remap(in_t, b_start.time, b_end.time, 0, 1)
	var start_pos := b_start.position
	var handle_pos_1 := b_start.handle_out_pos
	var handle_pos_2 := b_end.handle_in_pos
	var end_pos := b_end.position
	var start_rot := b_start.rotation
	var end_rot := b_end.rotation
	var start_scale := b_start.scale
	var end_scale := b_end.scale
	
	var lerp_1 = start_pos.lerp(handle_pos_1, b_t)
	var lerp_2 = handle_pos_1.lerp(handle_pos_2, b_t)
	var lerp_3 = handle_pos_2.lerp(end_pos, b_t)
	
	var lerp_second_1 := lerp_1.lerp(lerp_2, b_t)
	var lerp_second_2 := lerp_2.lerp(lerp_3, b_t)
	
	var final_pos := lerp_second_1.lerp(lerp_second_2, b_t)
	var final_pos_2 := lerp_second_1.lerp(lerp_second_2, b_t + 0.005)
	var base_rot := start_rot.slerp(end_rot, ease(b_t, -2))
	var forward_dir := (final_pos_2 - final_pos).normalized()
	var right_dir := base_rot.x
	var up_dir := forward_dir.cross(right_dir).normalized()
	var final_rot := Basis(right_dir, up_dir, forward_dir).orthonormalized()
	var angle := final_rot.z.signed_angle_to(forward_dir, up_dir)
	final_rot = final_rot.rotated(up_dir, angle)
	#DebugDraw3D.draw_arrow(final_pos, final_pos + final_rot.x * 8, Color.RED, 0.25, true, 0.5)
	#DebugDraw3D.draw_arrow(final_pos, final_pos + final_rot.y * 8, Color.GREEN, 0.25, true, 0.5)
	#DebugDraw3D.draw_arrow(final_pos, final_pos + final_rot.z * 8, Color.BLUE, 0.25, true, 0.5)
	
	#DebugDraw3D.draw_arrow(final_pos, final_pos + right_dir * 8, Color.RED, 0.25, true, 0.5)
	#DebugDraw3D.draw_arrow(final_pos, final_pos + up_dir * 8, Color.GREEN, 0.25, true, 0.5)
	#DebugDraw3D.draw_arrow(final_pos, final_pos + forward_dir * 8, Color.BLUE, 0.25, true, 0.5)
	#base_rot = base_rot.rotated(up_dir, angle)
	#var final_rot := Basis(right_dir, up_dir, forward_dir).orthonormalized()
	var final_scale := start_scale.lerp(end_scale, ease(b_t, -2))
	
	var final_transform := Transform3D(final_rot, final_pos).scaled_local(final_scale)
	if road_curve.global_transformations.size() > 0:
		var inv_global_transform := road_curve.global_transformations[0].get_root_transform(in_t)
		final_transform = inv_global_transform.inverse() * final_transform
	return final_transform

func calculate_curves_from_bezier(subdivision : int = 1024) -> void:
	road_curve.clear_all_keyframes()
	for i in subdivision:
		var fraction := (1.0 / (subdivision - 1)) * i
		var final_transform = sample_bezier_at_time(fraction)
		road_curve.key_transform_at_time(final_transform.origin, final_transform.basis, final_transform.basis.get_scale(), fraction)
	#road_curve.auto_smooth_all_keys()
	for gizmo:EditorNode3DGizmo in get_gizmos():
		gizmo._redraw()

func sort_beziers() -> void:
	bezier_points.sort_custom(func(a:BezierControlPoint, b:BezierControlPoint):
		return a.time < b.time)

func add_bezier_point(in_bezier : BezierControlPoint) -> void:
	bezier_points.append(in_bezier)
	if bezier_points.size() >= 2:
		sort_beziers()
		refresh_handle_nodes()
		calculate_curves_from_bezier()

func remove_bezier_point_at_index(in_index : int) -> void:
	bezier_points.remove_at(in_index)
	bezier_points[0].time = 0
	bezier_points[bezier_points.size() - 1].time = 1
	if bezier_points.size() >= 2:
		sort_beziers()
		refresh_handle_nodes()
		calculate_curves_from_bezier()

func _ready():
	road_curve.position_x.bake_resolution = 32
	road_curve.position_y.bake_resolution = 32
	road_curve.position_z.bake_resolution = 32
	road_curve.rotation_xx.bake_resolution = 32
	road_curve.rotation_xy.bake_resolution = 32
	road_curve.rotation_xz.bake_resolution = 32
	road_curve.rotation_yx.bake_resolution = 32
	road_curve.rotation_yy.bake_resolution = 32
	road_curve.rotation_yz.bake_resolution = 32
	road_curve.rotation_zx.bake_resolution = 32
	road_curve.rotation_zy.bake_resolution = 32
	road_curve.rotation_zz.bake_resolution = 32
	road_curve.scale_x.bake_resolution = 32
	road_curve.scale_y.bake_resolution = 32
	road_curve.scale_z.bake_resolution = 32
	_try_generate_mesh()
	if !Engine.is_editor_hint():
		return
	if bezier_points.size() >= 2:
		sort_beziers()
		refresh_handle_nodes()
		calculate_curves_from_bezier()

var awaiting := false

func refresh_handle_nodes() -> void:
	if awaiting:
		return
	for node in bezier_handle_nodes:
		node.queue_free()
	for child in get_children(true):
		if !(child is MeshInstance3D):
			child.queue_free()
	bezier_handle_nodes.clear()
	awaiting = true
	await get_tree().create_timer(0.01).timeout
	awaiting = false
	for i in bezier_points.size():
		var point_handle := BezierHandle.new()
		point_handle.position = bezier_points[i].position
		point_handle.basis = bezier_points[i].rotation
		point_handle.scale = bezier_points[i].scale
		point_handle.in_handle_length = bezier_points[i].handle_in
		point_handle.out_handle_length = bezier_points[i].handle_out
		point_handle.time = bezier_points[i].time
		add_child(point_handle)
		point_handle.owner = get_tree().get_edited_scene_root()
		bezier_handle_nodes.append(point_handle)
		point_handle.name = "Point " + str(i + 1)
		#print(i + 1)

var point_changes := false

func _process(delta):
	if !(road_shape and road_curve):
		return
	if !(road_curve.position_x):
		return
	if !Engine.is_editor_hint():
		return
	if bezier_points.size() < 2:
		return
	if bezier_points.size() >= 2 and bezier_handle_nodes.size() == 0:
		refresh_handle_nodes()
	else:
		if !bezier_handle_nodes[0].name.begins_with("Point"):
			refresh_handle_nodes()
	#refresh_handle_nodes()
	var total_dist := 0.0
	var dist_so_far := 0.0
	for i in bezier_points.size() - 1:
		var p1 := bezier_points[i].position
		var p2 := bezier_points[i + 1].position
		total_dist += p1.distance_to(p2)
	for i in bezier_points.size():
		var cp_pos := bezier_handle_nodes[i].position
		var cp_rot := bezier_handle_nodes[i].basis
		if !(bezier_points[i].position == bezier_handle_nodes[i].position):
			point_changes = true
		if !(bezier_points[i].rotation == bezier_handle_nodes[i].basis.orthonormalized()):
			point_changes = true
		if !(bezier_points[i].scale == bezier_handle_nodes[i].scale):
			point_changes = true
		if !(bezier_points[i].handle_in == bezier_handle_nodes[i].in_handle_length):
			point_changes = true
		if !(bezier_points[i].handle_out == bezier_handle_nodes[i].out_handle_length):
			point_changes = true
		bezier_points[i].position = bezier_handle_nodes[i].position
		bezier_points[i].rotation = bezier_handle_nodes[i].basis.orthonormalized()
		bezier_points[i].scale = bezier_handle_nodes[i].scale
		bezier_points[i].handle_in = bezier_handle_nodes[i].in_handle_length
		bezier_points[i].handle_out = bezier_handle_nodes[i].out_handle_length
		#bezier_points[i].time = bezier_handle_nodes[i].time
		if i != 0 and i != bezier_points.size() - 1:
			var ratio := dist_so_far / total_dist
			#print(ratio)
			bezier_points[i].time = lerpf(0, 1, ratio)
			#print("changing time to " + str(bezier_points[i].time))
		if i < bezier_points.size() - 1:
			var p1 := bezier_points[i].position
			var p2 := bezier_points[i + 1].position
			dist_so_far += p1.distance_to(p2)
		var ddo := bezier_points[i].rotation * 16
		var p2 := bezier_handle_nodes[i].position
		var tf := Transform3D(ddo, bezier_handle_nodes[i].position)
		#DebugDraw3D.scoped_config().set_thickness(0.1)
		#DebugDraw3D.draw_line(p2, p1, Color(0.6, 0.6, 0.6), delta)
		#DebugDraw3D.draw_sphere(tf.origin, 1.6, Color.WHITE, delta)
		#DebugDraw3D.draw_line(p2, p3, Color(0.6, 0.6, 0.6), delta)
		#DebugDraw3D.draw_sphere(p1, 1.2, Color.WHITE, delta)
		#DebugDraw3D.draw_sphere(p3, 1.2, Color.WHITE, delta)
	
	#var num_rows := 5
	#for i in num_rows:
		#var row := (1.0 / (num_rows - 1)) * i * 2.0 - 1.0
		#var span := sin(0.001 * Time.get_ticks_msec()) * 0.5 + 0.5
		#var point_transform := road_shape.get_transform_at_time(road_curve, Vector2(row, span))
		#DebugDraw3D.draw_sphere(point_transform.origin, 0.5, Color.RED, delta)
	
	if Time.get_ticks_msec() > last_gen_time + 250:
		if !point_changes:
			return
		point_changes = false
		calculate_curves_from_bezier(64)
		_try_generate_mesh()
		last_gen_time = Time.get_ticks_msec()
	for gizmo:EditorNode3DGizmo in get_gizmos():
		gizmo._redraw()
